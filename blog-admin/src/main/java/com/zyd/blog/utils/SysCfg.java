package com.zyd.blog.utils;

import java.io.File;

/**
 * 系统总配置类
 */
public class SysCfg {


    public static int likeListCount = 40; // 说说点赞、转发、浏览等记录的记录冗余数，注：过多会影响效率

    public static String pathHttp;  // 记录http地址



    
    // 图片上传相关参数
    
    public static String up_static_path = "upload_file/";	// 上传总文件夹
    public static String up_abs_path;	// 绝对路径，[待求]
    public static String up_img_path = "img/";		// 图片文件夹
    public static String up_video_path = "video/";		// 视频文件夹
    public static final String up_http_dyc = "dyc/";			// 动态路由前缀
    public static long up_max_size = 1024 * 1024 * 1024 ;	//文件最大大小,单位/B
    public static String up_img_gs = "jpg,JPG,jpeg,png,PNG,gif,GIF,ico,bmp";		// 图片允许的后缀集合
    public static String up_video_gs = "mp4,MP4,avi,flv,rmvb";		// 视频允许的后缀集合
    public static int take_frame = 100;		// 视频提取封面时指定帧（太小的帧容易是全黑画面）


    
    
    
    
    
    
    
    
    
    // 创建路径
    static{
    	up_abs_path = new File(up_static_path).getAbsolutePath() + "/";
		System.out.println("初始化的绝对地址："+up_abs_path);
		File f = new File(up_abs_path + up_http_dyc + up_img_path);
		if(f.exists() == false){
			f.mkdirs();
			System.out.println("已经创建地址：" + f.getAbsolutePath());
		}
		File f2 = new File(up_abs_path + up_http_dyc + up_video_path);
		if(f2.exists() == false){
			f2.mkdirs();
			System.out.println("已经创建地址：" + f2.getAbsolutePath());
		}
    }
    
    
    
    
    
    
    

}
