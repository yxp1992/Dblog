package com.zyd.blog.utils;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AtUtil {

    // 返回指定内容里的userId集合
    public static List<Long> getSubUserId(String content) {
        String rgex = "user-id=\"(.*?)\"";
        List<Long> list = new ArrayList<Long>();
        Pattern pattern = Pattern.compile(rgex);// 匹配的模式 sa
        Matcher m = pattern.matcher(content);
        while (m.find()) {
            int i = 1;
            Long s =  Long.parseLong(m.group(i));
            if (!list.contains(s)) {
                list.add(s);
            }
            i+=1;
        }
        return list;
    }

    /**
     * 返回两个集合的交集
     * @param listA 集合A
     * @param listB 集合B
     * @return
     */
    public static List<Long> getListIntersection(List<Long> listA, List<Long> listB){
        List<Long> listC = new ArrayList();
        for (Long item: listA ) {
            if(listB.contains(item)){
                listC.add(item);
            }
        }
        return listC;
    }


}
