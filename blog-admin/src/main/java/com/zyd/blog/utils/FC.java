package com.zyd.blog.utils;

import com.zyd.blog.mapper.*;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Spring依赖注入工厂类.所有Mapper
 */
@Component
public class FC {

    /** 公共Mapper */
    public static PublicMapper publicMapper;
    @Autowired
    public void setPublicMapper(PublicMapper publicMapper) {
        FC.publicMapper = publicMapper;
    }


    // 用户表Mapper接口
    public static SysUserMapper sysUserMapper;
    @Autowired
    public void setSysUserMapper(SysUserMapper sysUserMapper) {
        FC.sysUserMapper = sysUserMapper;
    }


    // 用户表关系表Mapper接口
    public static ManyUserUserMapper manyUserUserMapper;
    @Autowired
    public void setManyUserUserMapper(ManyUserUserMapper manyUserUserMapper) {
        FC.manyUserUserMapper = manyUserUserMapper;
    }

    // 角色表 Mapper
    public static SysRoleMapper sysRoleMapper;
    @Autowired
    public void setSysRoleMapper(SysRoleMapper sysRoleMapper) {
        FC.sysRoleMapper = sysRoleMapper;
    }

    // 权限表 Mapper
    public static SysJurMapper sysJurMapper;
    @Autowired
    public void setSysJurMapper(SysJurMapper sysJurMapper) {
        FC.sysJurMapper = sysJurMapper;
    }




    // 说说Mapper接口
    public static BizSmalltalkMapper bizSmalltalkMapper;
    @Autowired
    public void setBizSmalltalkMapper(BizSmalltalkMapper bizSmalltalkMapper) {
        FC.bizSmalltalkMapper = bizSmalltalkMapper;
    }


    // 说说评论Mapper接口
    public static BizSmalltalkCommentMapper bizSmalltalkCommentMapper;
    @Autowired
    public void setBizSmalltalkCommentMapper(BizSmalltalkCommentMapper bizSmalltalkCommentMapper) {
        FC.bizSmalltalkCommentMapper = bizSmalltalkCommentMapper;
    }


    // 消息通知Mapper接口
    public static SysMessageMapper sysMessageMapper;
    @Autowired
    public void setSysMessageMapper(SysMessageMapper sysMessageMapper) {
        FC.sysMessageMapper = sysMessageMapper;
    }


    // 申请回复Mapper接口
    public static BizApplyMapper bizApplyMapper;
    @Autowired
    public void setBizApplyMapper(BizApplyMapper bizApplyMapper) {
        FC.bizApplyMapper = bizApplyMapper;
    }


    // 法学院视频 Mapper接口
    public static BizLawSchoolVideoMapper bizLawSchoolVideoMapper;
    @Autowired
    public void setBizLawSchoolVideoMapper(BizLawSchoolVideoMapper bizLawSchoolVideoMapper) {
        FC.bizLawSchoolVideoMapper = bizLawSchoolVideoMapper;
    }
    
    // 法学院视频评论 Mapper接口
    public static BizLawSchoolCommentMapper bizLawSchoolCommentMapper;
    @Autowired
    public void setBizLawSchoolCommentMapper(BizLawSchoolCommentMapper bizLawSchoolCommentMapper) {
        FC.bizLawSchoolCommentMapper = bizLawSchoolCommentMapper;
    }


    // 首页轮播图 Mapper接口
    public static SysLoopPlayMapper sysLoopPlayMapper;
    @Autowired
    public void setSysLoopPlayMapper(SysLoopPlayMapper sysLoopPlayMapper) {
        FC.sysLoopPlayMapper = sysLoopPlayMapper;
    }

    
    
    
}
