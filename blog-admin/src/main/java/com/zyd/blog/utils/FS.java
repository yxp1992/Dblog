package com.zyd.blog.utils;

import com.zyd.blog.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Spring依赖注入工厂类.所有Service
 */
@Component
@Slf4j
public class FS {


    /**
     * 如何true，则抛出一个运行时异常
     * @param isThrow
     * @param str
     * @throws RuntimeException
     */
    public static void throwRuntimeException(boolean isThrow,  String str, String logStr) throws RuntimeException{
        if(isThrow){
            if(logStr != null){
                log.info(logStr);
            }
            throw new RuntimeException(str);
        }
    }

    public static void throwRuntimeException( String str, String logStr) throws RuntimeException{
        if(logStr != null){
            log.info(logStr);
        }
        throw new RuntimeException(str);
    }


    // 说说业务服务类 --end
    public static BizSmalltalkService bizSmalltalkService;
    @Autowired
    public void setBizSmalltalkService(BizSmalltalkService bizSmalltalkService) {
        FS.bizSmalltalkService = bizSmalltalkService;
    }

    // 用户表，关系接口
    public static SysUserService sysUserService;
    @Autowired
    public void setSysUserService(SysUserService sysUserService) {
        FS.sysUserService = sysUserService;
    }

    //用户关系 业务接口
    public static ManyUserUserService manyUserUserService;
    @Autowired
    public void setManyUserUserService(ManyUserUserService manyUserUserService) {
        FS.manyUserUserService = manyUserUserService;
    }

    // 角色 业务类
    public static SysRoleService sysRoleService;
    @Autowired
    public void setSysRoleService(SysRoleService sysRoleService) {
        FS.sysRoleService = sysRoleService;
    }


    // 申请与回复 业务接口
    public static BizApplyService bizApplyService;
    @Autowired
    public void setBizApplyService(BizApplyService bizApplyService) {
        FS.bizApplyService = bizApplyService;
    }


    // 账号登陆 业务接口
    public static AccService accService;
    @Autowired
    public void setBizApplyService(AccService accService) {
        FS.accService = accService;
    }

    

    // 法学院视频 Service接口
    public static BizLawSchoolVideoService bizLawSchoolVideoService;
    @Autowired
    public void setBizLawSchoolVideoService(BizLawSchoolVideoService bizLawSchoolVideoService) {
        FS.bizLawSchoolVideoService = bizLawSchoolVideoService;
    }
    
    // 法学院视频评论 Service接口
    public static BizLawSchoolCommentService bizLawSchoolCommentService;
    @Autowired
    public void setBizLawSchoolCommentService(BizLawSchoolCommentService bizLawSchoolCommentService) {
        FS.bizLawSchoolCommentService = bizLawSchoolCommentService;
    }


    // 首页轮播 Service接口
    public static SysLoopPlayService sysLoopPlayService;
    @Autowired
    public void setSysLoopPlayService(SysLoopPlayService sysLoopPlayService) {
        FS.sysLoopPlayService = sysLoopPlayService;
    }

    



}
