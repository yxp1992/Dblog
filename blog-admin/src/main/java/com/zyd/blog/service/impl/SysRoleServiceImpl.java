package com.zyd.blog.service.impl;

import com.zyd.blog.model.SysRole;
import com.zyd.blog.service.SysRoleService;
import com.zyd.blog.utils.FC;
import com.zyd.blog.utils.PageAndData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SysRoleServiceImpl implements SysRoleService {


    // 写如一条
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int save(SysRole sysRole){
        FC.sysRoleMapper.save(sysRole);
        return (int)FC.publicMapper.getPkId();
    }

    // 修改一条
    @Override
    public int update(SysRole sysRole){
        return FC.sysRoleMapper.update(sysRole);
    }

    // 返回角色列表
    @Override
    public PageAndData getRoleList(){
        return PageAndData.get(null, FC.sysRoleMapper.getList());
    }


    // ================ 权限相关 ========================

    // 返回权限列表
    @Override
    public PageAndData getJurList(){
        return PageAndData.get(null, FC.sysJurMapper.getList(0));
    }


    // 返回指定id角色的所有权限列表，int形式
    @Override
    public List<Integer> getJurListInt(int role_id){
        return FC.sysJurMapper.getListInt(role_id);
    }

    // 修改权限为指定权限列表
    @Override
    public int saveRoleJurList(int role_id, int[] jur_id){
        for(int jid : jur_id){
            FC.sysJurMapper.saveRoleJur(role_id, jid);
        }
        return jur_id.length;
    }





}
