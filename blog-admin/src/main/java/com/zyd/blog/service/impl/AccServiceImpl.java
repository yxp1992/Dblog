/**
 * MIT License
 * Copyright (c) 2018 yadong.zhang
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.zyd.blog.service.impl;

import com.zyd.blog.model.SysUser;
import com.zyd.blog.service.AccService;
import com.zyd.blog.service.SysUserService;
import com.zyd.blog.utils.FC;
import com.zyd.blog.utils.NbUtil;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.webutil.Md5Util;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 账号登陆相关业务
 */
@Service
public class AccServiceImpl implements AccService {

    // 处理登录
    @Override
    public RedJson doLogin(String username, String password, String login_ip) {
        SysUser sysUser = FC.sysUserMapper.getByUsername(username);
        if(sysUser == null){
            return RedJson.getJsonError("无此用户");
        }
        String MD5_PWD = Md5Util.MD5(password);
        if(sysUser.getPassword().equals(MD5_PWD) == false){
            return RedJson.getJsonError( "密码错误");
        }
        if(sysUser.getStatus() == 2){
            return RedJson.getJsonError("此帐号已被禁用");
        }
        // 到此已经代表登陆成功，手动更新数据，这样就不用再去数据库查一次了，节省时间
        FC.sysUserMapper.successLogin(sysUser.getId(), login_ip);
        sysUser.setLast_login_ip(login_ip);     // ip
        sysUser.setLast_login_time(new Date()); // 时间
        sysUser.setLogin_count(sysUser.getLogin_count()+1); // 登陆次数
        sysUser.setPassword("********");    // 销毁密码
        return RedJson.getJsonSuccess("成功").setData(sysUser);
    }



}
