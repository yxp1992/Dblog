package com.zyd.blog.service;

import com.zyd.blog.model.SysLoopPlay;
import com.zyd.blog.utils.PageAndData;

/**
 * <p>
 * 轮播图管理表 服务类
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-03
 */
public interface SysLoopPlayService {

    /**
     * 添加一条
     */
    public long add(SysLoopPlay sysLoopPlay);


    /**
     * 删除一条，根据主键
     */
    public int deleteById(Long id);

    /**
     * 修改一条记录(标题、图片地址、超链接跳转地址、条件id)
     */
    public int update(SysLoopPlay sysLoopPlay);

    /**
     * 查询,根据条件
     * @param type 类型(1=首页顶部的,2=……)
     * @param is_enable 是否启用
     * @return
     */
    public PageAndData getList(int pageNo, int pageSize, int type, int is_enable);


}
