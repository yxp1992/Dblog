package com.zyd.blog.service;


import com.zyd.blog.model.BizLawSchoolVideo;
import com.zyd.blog.model.so.BizLawSchoolVideoSO;
import com.zyd.blog.utils.PageAndData;

/**
 * <p>
 * 法学院视频表 服务类
 * </p>
 */
public interface BizLawSchoolVideoService{
   
    /**
     * 查询，根据参数条件 
     */
    public PageAndData getListByParam(BizLawSchoolVideoSO bizLawSchoolVideoSO);


    /**
     * 根据ID获取
     */
    public BizLawSchoolVideo getById(Long id);

    /**
     * 发布一条法库，需要提供：
     *  title 标题
     *  videoUrl 视频地址
     *  coverUrl 封面地址
     *  type 类型
     *  createUserid 创建人userid
     * @return
     */
    public Long save(BizLawSchoolVideo bizLawSchoolVideo);

    /**
     * 删除一条，根据主键
     */
    public int deleteById(Long id);

}
