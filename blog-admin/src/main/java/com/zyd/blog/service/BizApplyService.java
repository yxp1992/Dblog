package com.zyd.blog.service;

import com.zyd.blog.model.BizApply;
import com.zyd.blog.utils.Page;
import com.zyd.blog.utils.PageAndData;

/**
 * <p>
 *  说说与回复 业务接口
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-06-27
 */
public interface BizApplyService {


    // 根据id返回
    public BizApply getById(long id);

    // 指定uid 发布一条申请,返回插入的记录数量
    public int add(Long userId, String content);


    // 处理一条申请
    public int handleApply(Long id, int status, String handContent);



    /**
     * 查询
     * @param page 分页
     * @param userId 指定用户的数据
     * @param way 方式(1=获取我的申请 ,2=获取我的回复)
     * @param is_hand 是否已经处理 （1=是，2=否）
     * @return
     */
    public PageAndData getByWayAndStatus(Page page, Long userId, int way, int is_hand);




}
