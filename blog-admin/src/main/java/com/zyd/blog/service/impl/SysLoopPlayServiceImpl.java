package com.zyd.blog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zyd.blog.mapper.SysLoopPlayMapper;
import com.zyd.blog.model.SysLoopPlay;
import com.zyd.blog.service.SysLoopPlayService;
import com.zyd.blog.utils.FC;
import com.zyd.blog.utils.Page;
import com.zyd.blog.utils.PageAndData;

/**
 * <p>
 * 轮播图管理表 服务实现类
 * </p>
 *
 * @author yuxiaopeg
 * @since 2018-07-03
 */
@Service
public class SysLoopPlayServiceImpl implements SysLoopPlayService {

    @Autowired
    SysLoopPlayMapper sysLoopPlayMapper;

    /**
     * 添加一条
     */
    @Override
    public long add(SysLoopPlay sysLoopPlay) {
    	FC.sysLoopPlayMapper.save(sysLoopPlay);
        return FC.publicMapper.getPkId();
    }

    /**
     * 删除一条，根据主键
     */
    @Override
    public int deleteById(Long id) {
        return sysLoopPlayMapper.deleteById(id);
    }

    /**
     * 修改一条记录(标题、图片地址、超链接跳转地址、条件id)
     */
    @Override
    public int update(SysLoopPlay sysLoopPlay) {
        return sysLoopPlayMapper.update(sysLoopPlay);
    }

    /**
     * 查询,根据条件
     * @param type 类型(1=首页顶部的,2=……)
     * @param is_enable 是否启用(1=是,2=否)
     * @return
     */
    @Override
    public PageAndData getList(int pageNo, int pageSize, int type, int is_enable) {
    	com.github.pagehelper.Page<?> pagePlug = com.github.pagehelper.PageHelper.startPage(pageNo, pageSize);
        List<?> data = FC.sysLoopPlayMapper.getList(type, is_enable);
        Page page = new Page(pageNo, pageSize).setCount(pagePlug.getTotal());
        return PageAndData.get(page, data);
    }
}
