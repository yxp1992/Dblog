/**
 * MIT License
 * Copyright (c) 2018 yadong.zhang
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.zyd.blog.service;


import com.zyd.blog.model.SysRole;
import com.zyd.blog.utils.PageAndData;
import com.zyd.blog.utils.RedJson;

import java.util.List;

/**
 * 角色 业务类
 */
public interface SysRoleService {


    // 写如一条
    public int save(SysRole sysRole);

    // 修改一条
    public int update(SysRole sysRole);

    // 返回角色列表
    public PageAndData getRoleList();


    // ================ 权限相关 ========================


    // 返回权限列表
    public PageAndData getJurList();


    // 返回指定id角色的所有权限列表，int形式
    public List<Integer> getJurListInt(int role_id);


    // 修改权限为指定权限列表
    public int saveRoleJurList(int role_id, int[] jur_id);





}
