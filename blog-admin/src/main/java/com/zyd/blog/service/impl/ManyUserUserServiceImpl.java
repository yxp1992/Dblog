/**
 * MIT License
 * Copyright (c) 2018 yadong.zhang
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.zyd.blog.service.impl;

import com.zyd.blog.model.SysMessage;
import com.zyd.blog.model.SysUser;
import com.zyd.blog.service.ManyUserUserService;
import com.zyd.blog.service.SysUserService;
import com.zyd.blog.utils.FC;
import com.zyd.blog.webutil.JurUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 用户关系表接口
 */
@Service
public class ManyUserUserServiceImpl implements ManyUserUserService {

    // 写入关注
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int save(long uid, long to_uid){
        String username = JurUtil.getUsername(uid);
        SysMessage sysMessage = new SysMessage(uid, to_uid,username + "关注了你",11, uid);
        FC.sysMessageMapper.save(sysMessage);   // 通知入库
        return FC.manyUserUserMapper.save(uid, to_uid); // 数据入库
    }

    // 删除关注
    @Override
    public int delete(long uid, long to_uid){
        return FC.manyUserUserMapper.delete(uid, to_uid);
    }

    // 是否有此关注消息
    @Override
    public int getByUidUid(long uid, long to_uid){
        return FC.manyUserUserMapper.getByUidUid(uid, to_uid);
    }



}
