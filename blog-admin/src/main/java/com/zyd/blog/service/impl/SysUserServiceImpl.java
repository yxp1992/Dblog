/**
 * MIT License
 * Copyright (c) 2018 yadong.zhang
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.zyd.blog.service.impl;

import com.github.pagehelper.PageHelper;
import com.zyd.blog.model.BizApply;
import com.zyd.blog.model.SysUser;
import com.zyd.blog.model.SysUserInfo;
import com.zyd.blog.service.SysUserService;
import com.zyd.blog.utils.FC;
import com.zyd.blog.utils.Page;
import com.zyd.blog.utils.PageAndData;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 用户
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    // 修改个人基本资料
    @Override
    public int updateInfo(SysUser sysUser){
        return FC.sysUserMapper.updateInfo(sysUser);
    }


    // 根据 Id 获取
    @Override
    public SysUser getById(long id){
        return FC.sysUserMapper.getById(id);
    }

    // 根据 username 获取
    @Override
    public SysUser getByUsername(String username){
        return FC.sysUserMapper.getByUsername(username);
    }

    // 返回指定id的各种count数据
    // apply_count=申请总数，reply_count=回复总数，follow_count=关注总数，fans_count=粉丝总数，release_count=发布说说总数
    @Override
    public Map getCountById(long id){
        return FC.sysUserMapper.getCountById(id);
    }

    // 获取指定id的关注
    public PageAndData getFollowUser(Page page, long id, int is_fans){
        com.github.pagehelper.Page pagePlug = PageHelper.startPage(page.getPageNo(), page.getPageSize());
        List<SysUserInfo> data = FC.sysUserMapper.getFollowUser(id, is_fans);
        return PageAndData.get(page.setCount(pagePlug.getTotal()), data);
    }

    // 指定id的粉丝
    public PageAndData getFansUser(Page page, long id){
        com.github.pagehelper.Page pagePlug = PageHelper.startPage(page.getPageNo(), page.getPageSize());
        List<SysUserInfo> data = FC.sysUserMapper.getFansUser(id);
        return PageAndData.get(page.setCount(pagePlug.getTotal()), data);
    }






}
