package com.zyd.blog.service.impl;

import com.zyd.blog.service.BizLawLibService;
import com.zyd.blog.model.BizLawLib;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-06-29
 */
@Service
public class BizLawLibServiceImpl implements BizLawLibService {

//    @Autowired
//    BizLawLibMapper bizLawLibMapper;    // Mapper 接口


    /**
     * 根据类型获取
     * @param type (1=法律法规,2=司法解释,3=经典案例,4=办案食物)
     * @param start_time 开始时间
     * @param end_time 结束时间
     * @return
     */
    @Override
    //@Transactional(rollbackFor = Exception.class)
    public List<BizLawLib> getListByType(Long type, String start_time, String end_time){
        return null;//bizLawLibMapper.getListByType(type,start_time,end_time,20);
    }


    /**
     * 根据ID获取
     */
    @Override
    //@Transactional(rollbackFor = Exception.class)
    public BizLawLib getById(Long id){
        return null;//bizLawLibMapper.getById(id);
    }

    /**
     * 发布一条法库
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long addLawLib(String title, String content, Long type, String cover_url, Long create_userid) {
        BizLawLib bizLawLib = new BizLawLib();
        bizLawLib.setTitle(title);      // 标题
        bizLawLib.setContent(content);  // 内容
        bizLawLib.setType(type);        // 类型
        bizLawLib.setCoverUrl(cover_url);   // 封面地址
        bizLawLib.setCreateUserid(create_userid); // 创建人uid
        bizLawLib.setCreateTime(new Date());
        bizLawLib.setUpdateTime(new Date());
        bizLawLib.setIsDelete(0);   //是否刪除
        // bizLawLibMapper.insert(bizLawLib);
        Long pkId = 1L;//bizLawLibMapper.getPkId();  //获得主键
        return pkId;
    }

    /**
     * 删除一条，根据主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteById(Long id){
        return 1;//bizLawLibMapper.deleteById(id);
    }


}
