package com.zyd.blog.service;

import com.zyd.blog.model.BizLawLib;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  法库 业务接口 -- end
 * </p>
 *
 */
@Service
public interface BizLawLibService {
    /**
     * 根据类型获取
     * @param type (1=法律法规,2=司法解释,3=经典案例,4=办案食物)
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @return
     */
    public List<BizLawLib> getListByType(Long type,String startDate,String endDate);


    /**
     * 根据ID获取
     */
    public BizLawLib getById(Long id);


    /**
     * 发布一条法库
     */
    public Long addLawLib(String title,String content,Long type,String cover_url,Long create_userid);

    /**
     * 删除一条，根据主键
     */
    public int deleteById(Long id);

}
