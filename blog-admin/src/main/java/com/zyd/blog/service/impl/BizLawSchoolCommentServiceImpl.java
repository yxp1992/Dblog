package com.zyd.blog.service.impl;

import com.zyd.blog.service.BizLawSchoolCommentService;
import com.zyd.blog.model.BizLawSchoolComment;
import com.zyd.blog.mapper.BizLawSchoolCommentMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 法学院视频评论表 服务实现类
 * </p>
 */
@Slf4j
@Service
public class BizLawSchoolCommentServiceImpl implements BizLawSchoolCommentService {


    @Autowired
    BizLawSchoolCommentMapper bizLawSchoolCommentMapper;    // 法学院视频评论表 Mapper接口


    /**
     * 增加一条评论
     * @param content 内容
     * @param video_id 所属视频id
     * @param create_userid 创建人id
     */
    @Override
    public void add(String content, Long video_id, Long create_userid) {
        BizLawSchoolComment bizLawSchoolComment = new BizLawSchoolComment();
        bizLawSchoolComment.setContent(content);    // 评论内容
        bizLawSchoolComment.setVideoId(video_id+"");   // 所属视频id
        bizLawSchoolComment.setCreateUserid(create_userid); // 创建人userid
        bizLawSchoolComment.setCreateTime(new Date()); //创建时间
        int a = bizLawSchoolCommentMapper.save(bizLawSchoolComment);  // 入库
        log.info("视频评论入库成功：" + a);
    }

    /**
     * 删除一条,根据id
     */
    @Override
    public int deleteById(Long id) {
        return bizLawSchoolCommentMapper.deleteById(id);
    }

    /**
     * 查询,根据条件
     * @param video_id 所属视频ID
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @param pageSize 页大小
     * @return
     */
    @Override
    public List<BizLawSchoolComment> getListByCondition(Long video_id, String startDate, String endDate, int pageSize) {
        return bizLawSchoolCommentMapper.getListByCondition(video_id,startDate,endDate,pageSize);
    }
}
