package com.zyd.blog.service.impl;

import com.github.pagehelper.PageHelper;
import com.zyd.blog.service.BizApplyService;
import com.zyd.blog.model.BizApply;
import com.zyd.blog.utils.Page;
import com.zyd.blog.utils.AtUtil;
import com.zyd.blog.utils.FC;
import com.zyd.blog.utils.PageAndData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  申请与回复 --业务接口实现类
 * </p>
 */
@Slf4j
@Service
public class BizApplyServiceImpl implements BizApplyService {

    // 根据id返回
    @Override
    public BizApply getById(long id){
        return FC.bizApplyMapper.getById(id);
    }

    // 指定uid 发布一条申请,返回插入的记录数量
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int add(Long userId, String content){
        int a = 0;
        List<Long> atUserIdList = AtUtil.getSubUserId(content);
        for(Long at_uid: atUserIdList){
            BizApply bizApply = new BizApply();
            bizApply.setCreate_uid(userId);
            bizApply.setContent(content);
            bizApply.setTo_uid(at_uid);
            FC.bizApplyMapper.save(bizApply);
            a += 1;
        }
        return a;
    }


    // 处理一条申请
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int handleApply(Long id, int status, String handContent){
        BizApply bizApply = new BizApply();
        bizApply.setId(id);
        bizApply.setStatus(status);
        bizApply.setHand_content(handContent);
        return FC.bizApplyMapper.handApply(bizApply);
    }



    /**
     * 查询
     * @param page 分页
     * @param userId 指定用户的数据
     * @param way 方式(1=获取我的申请 ,2=获取我的回复)
     * @param is_hand 是否已经处理 （1=是，2=否）
     * @return
     */
    @Override
    public PageAndData getByWayAndStatus(Page page, Long userId, int way, int is_hand){
        com.github.pagehelper.Page pagePlug = PageHelper.startPage(page.getPageNo(), page.getStart());
        List<BizApply> data = FC.bizApplyMapper.getByWayAndStatus(userId, way, is_hand);
        System.out.println(pagePlug.getTotal());
        return PageAndData.get(page.setCount(pagePlug.getTotal()), data);
    }




}
