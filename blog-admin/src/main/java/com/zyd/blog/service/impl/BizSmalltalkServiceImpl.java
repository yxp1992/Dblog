package com.zyd.blog.service.impl;

import com.github.pagehelper.PageHelper;
import com.zyd.blog.model.*;
import com.zyd.blog.model.so.BizSmalltalkSO;
import com.zyd.blog.service.BizSmalltalkService;
import com.zyd.blog.utils.*;
import com.zyd.blog.webutil.JurUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

/**
 * <p>
 *  说说业务服务实现类 --end
 * </p>
 */
// extends ServiceImpl<BizSmalltalkMapper, BizSmalltalk>
@Service
@Slf4j
public class BizSmalltalkServiceImpl implements BizSmalltalkService {



    // 返回指定id的说说
    @Override
    public BizSmalltalk getById(Long id){
        return FC.bizSmalltalkMapper.getById(id);
    }

    // 删除一条说说
    @Override
    public int delete(Long id){
        return FC.publicMapper.deleteById("biz_smalltalk", "id", id);
    }



    // 添加一条记录，返回主键
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long add(BizSmalltalk bizSmalltalk) {

        int a = FC.bizSmalltalkMapper.save(bizSmalltalk);   // 说说入库
        Long pkid = FC.publicMapper.getPkId();

        int bc = 2, d = 1;
        if(bizSmalltalk.getOriginalId() != 0){  // 不为0代表是转发

            bc = addValueS(bizSmalltalk.getOriginalId(), bizSmalltalk.getUserId(), 3);    // 增加转发数与被转发记录

            long old_uid = FC.bizSmalltalkMapper.getById(bizSmalltalk.getOriginalId()).getUserId();
            SysMessage sysMessage = new SysMessage(bizSmalltalk.getUserId(),old_uid,
                    bizSmalltalk.getUsername() + "转发了你的说说", 2, pkid);
            d = FC.sysMessageMapper.save(sysMessage);   // 对原说说发表人消息通知
        }

        // 对@到的人通知
        List<Long> atUserIdList = AtUtil.getSubUserId(bizSmalltalk.getContent());
        for(Long uid: atUserIdList){
            // 开始通知

        }

        if(a + bc + d < 4){
            FS.throwRuntimeException("失败", "用户xxx说说发表失败");
        }
        return pkid;
    }


    // 指定说说ID点赞
    // way(1=点赞，2=浏览，3=转发)
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int doLike(long id, long userId) {

        int ab = addValueS(id, userId, 1);

        // 对其发表者的通知一下
        int c = 1;
        long create_uid = FC.bizSmalltalkMapper.getById(id).getUserId();      // 说说发表人id
        if(userId != create_uid){
            String username = JurUtil.getUsername(userId);  // 当前人名字
            SysMessage sysMessage = new SysMessage(userId, create_uid, username + "赞了你的说说", 3, id);
            c = FC.sysMessageMapper.save(sysMessage);
        }
        if(ab + c < 3){
            FS.throwRuntimeException("点赞失败", userId+"对说说" + id + "点赞失败");
        }
        return 1;
    }

    // 指定说说取消点赞
    @Override
    public int unLike (long id, long userId){
        return FC.publicMapper.addX("biz_smalltalk", "like_count", -1, "id", id);
    }


    // 获取集合
    // 排序方式(1=id倒序，2=发表日期倒序，3=点赞倒序，4=浏览数量倒序，5=转发数量倒序，6=评论数量倒序)
    @Override
    public PageAndData getList(BizSmalltalkSO so){
        com.github.pagehelper.Page<?> pagePlug = PageHelper.startPage(so.getPageNo(), so.getPageSize());
        List<BizSmalltalk> data = FC.bizSmalltalkMapper.getList(so);
        Page page = new Page(so.getPageNo(), so.getPageSize()).setCount(pagePlug.getTotal());
        return PageAndData.get(page, data);
    }

    // 获取指定id说说的转发列表
    @Override
    public PageAndData getForwardList(int pageNo, int pageSize, long id){
        com.github.pagehelper.Page<?> pagePlug = com.github.pagehelper.PageHelper.startPage(pageNo, pageSize);
        List<?> data = FC.bizSmalltalkMapper.getForwardList(id);
        Page page = new Page(pageNo, pageSize).setCount(pagePlug.getTotal());
        return PageAndData.get(page, data);
    }


    
    


    // ============================== 说说评论相关  =======================================

    // 插入一条说说评论 #{content}, #{smalltalk_id}, #{create_uid}, #{reply_userid}, #{reply_username}
    @Override
    @Transactional(rollbackFor = Exception.class)
    public long addComment(BizSmalltalkComment bizSmalltalkComment){

        // 先所需值
        long create_uid = bizSmalltalkComment.getCreate_uid();      // 评论人创建人
        BizSmalltalk bizSmalltalk = FC.bizSmalltalkMapper.getById(bizSmalltalkComment.getSmalltalk_id());   // 所评论的说说实体

        // 1、评论入库
        int a = FC.bizSmalltalkCommentMapper.save(bizSmalltalkComment);
        long pkid = FC.publicMapper.getPkId();

        // 2、原说说评论数+1
        int b = FC.publicMapper.addX("biz_smalltalk", "comment_count", 1, "id", bizSmalltalkComment.getSmalltalk_id());

        // 3、通知说说发表人(如果不是回复自己的话)
        int c = 1;
        if(create_uid != bizSmalltalk.getUserId()){
            SysMessage sysMessage = new SysMessage(create_uid, bizSmalltalk.getUserId(),
                    JurUtil.getUsername(create_uid) + "回复了你的说说", 1, bizSmalltalkComment.getSmalltalk_id());
            b = FC.sysMessageMapper.save(sysMessage);   // 消息通知入库
        }

        if( a + b + c < 3){
            FS.throwRuntimeException("说说评论失败", create_uid + "说说评论失败");
        }
        return pkid;
    }


    // 删除一条评论
    @Override
    public int delComment(long id){
        return FC.publicMapper.deleteById("biz_smalltalk_comment", "id", id);
    }

    // 拉取指定说说的评论
    @Override
    public PageAndData getCommentList(Page page, long sid, int orederType){
        com.github.pagehelper.Page pagePlug = PageHelper.startPage(page.getPageNo(), page.getPageSize());
        List<BizSmalltalkComment> data = FC.bizSmalltalkCommentMapper.getList(sid, orederType);
        return PageAndData.get(page.setCount(pagePlug.getTotal()), data);
    }


    // ==================    私有方法    ===============================================

    // 共用逻辑，增加记录与list缓存
    // 说书id，用户id,way(1=点赞，2=浏览，3=转发)
    private int addValueS(long id, long userId, int way){

        String countColumn = new String[]{"", "like_count", "browse_count", "forward_count"}[way];
        String listColumn = new String[]{"", "like_user_list", "browse_user_list", "forward_user_list"}[way];
        String tips = new String[]{"", "点赞", "浏览", "转发"}[way];

        int a = FC.publicMapper.addX("biz_smalltalk", countColumn, 1, "id", id);    // 数量+1

        // 更新最近记录
        String listString = FC.publicMapper.getColumn("biz_smalltalk", listColumn,"id", id);
        if(listString.split(",").length> SysCfg.likeListCount){
            // 如果太长，舍掉一般
            int index = listString.indexOf(",", listString.length()/2);
            listString = listString.substring(0,index+1);
        }
        String username = JurUtil.getUsername(userId);
        listString = userId + "、" + username + "," + listString;
        int b = FC.publicMapper.updateColumn("biz_smalltalk", listColumn, listString, "id", id);    // 更新最近列表

        return a + b;
    }






}


