package com.zyd.blog.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zyd.blog.model.BizLawSchoolVideo;
import com.zyd.blog.model.so.BizLawSchoolVideoSO;
import com.zyd.blog.service.BizLawSchoolVideoService;
import com.zyd.blog.utils.FC;
import com.zyd.blog.utils.Page;
import com.zyd.blog.utils.PageAndData;

/**
 * 法学院视频表 服务实现类
 */
@Service
public class BizLawSchoolVideoServiceImpl implements BizLawSchoolVideoService {


    /**
     * 发布一条法库，需要提供：
     *  title 标题
     *  videoUrl 视频地址
     *  coverUrl 封面地址
     *  type 类型
     *  createUserid 创建人userid
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long save(BizLawSchoolVideo bizLawSchoolVideo) {
        FC.bizLawSchoolVideoMapper.save(bizLawSchoolVideo);  //入库
        return FC.publicMapper.getPkId();
    }
    
    /**
     * 删除一条，根据主键
     */
    @Override
    public int deleteById(Long id) {
        return FC.publicMapper.deleteById("biz_law_school_video", "id", id);
    }
    
    /**
     * 根据ID获取
     */
    @Override
    public BizLawSchoolVideo getById(Long id) {
        return FC.bizLawSchoolVideoMapper.getById(id);
    }

    /**
     * 查询，根据参数条件
     */
    @Override
    public PageAndData getListByParam(BizLawSchoolVideoSO so){
    	com.github.pagehelper.Page<?> pagePlug = com.github.pagehelper.PageHelper.startPage(so.getPageNo(), so.getPageSize());
		List<BizLawSchoolVideo> data = FC.bizLawSchoolVideoMapper.getList(so);
		Page page = new Page(so.getPageNo(), so.getPageSize()).setCount(pagePlug.getTotal());
		return PageAndData.get(page, data);
    }


    
    
}
