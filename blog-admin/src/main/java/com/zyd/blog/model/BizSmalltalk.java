package com.zyd.blog.model;

import com.baomidou.mybatisplus.annotations.TableId;
import java.util.Date;
import com.baomidou.mybatisplus.enums.IdType;
import java.io.Serializable;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 说说实体类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BizSmalltalk implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Long userId;        // 用户ID
    private String content;     // 说说内容
    private String keywords;    // 说说关键字，优化搜索
    private Long originalId;    // 所转发的说说id，值为0代表并没有转发说说，是一个原创
    private String imgList;     // 附带图片列表
    private String videoList;     // 附带的视频列表，逗号间隔
    private int type;           // 被鉴别的类型(1=普通,2=图片,3=视频)
    private Date createTime;    // 添加时间
    private Date updateTime;    // 更新时间
    private int commentCount;   // 评论数量
    private int forwardCount;   // 转发数量
    private String forwardUserList; // 转发用户列表{userId:1,username:'张三'}
    private int likeCount;      // 点赞数量
    private String likeUserList; // 点赞用户列表{userId:1,username:'张三'}
    private int browseCount;    // 浏览数量
    private String browseUserList; // 浏览量用户列表{userId:1,username:'张三'}



    // 额外字段
    private String username;    // 说说发表人昵称
    private String avatar;      // 说说发表人头像地址



}
