package com.zyd.blog.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 角色表
 */
@Data
public class SysRole implements Serializable {


    private long id;
    private String role_name;   // 角色主键
    private String role_info;   // 角色详细描述
    private int is_lock;        // 是否锁定(1=是,2=否),锁定之后不可随意删除
    private Date create_time;   // 创建时间





}
