package com.zyd.blog.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 轮播图管理表
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysLoopPlay implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 图片地址
     */
    private String img_src;
    /**
     * 点击后跳转的超链接
     */
    private String a_href;
    /**
     * 是否启用(1=是,2=否)
     */
    private Integer is_enable;
    /**
     * 轮播图位置(1=首页顶部的,2=……)
     */
    private Integer type;
    /**
     * 创建时间
     */
    @JSONField(format="yyyy-MM-dd hh:mm")
    private Date create_time;


}
