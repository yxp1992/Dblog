package com.zyd.blog.model;

import com.baomidou.mybatisplus.annotations.TableId;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 法学院视频表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BizLawSchoolVideo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 视频url地址
     */
    private String video_url;
    /**
     * 封面url地址
     */
    private String cover_url;
    /**
     * 分类
     */
    private int type;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 观看数量
     */
    private Integer see_count;
    /**
     * 评论数量
     */
    private Integer comment_count;
    /**
     * 是否精品(2=否，1=是)
     */
    private Integer is_delicate;
    /**
     * 创建时间
     */
    private Date create_time;


}
