package com.zyd.blog.model.so;

import lombok.Data;

import java.io.Serializable;

/**
 * 说说表，查询条件封装
 */
@Data
public class BizLawSchoolVideoSO implements Serializable {

	private static final long serialVersionUID = -624520511757748423L;
	
	
	private int pageNo = 1;     // 当前页
    private int pageSize = 10;   // 页大小

    private String title;		// 标题模糊搜索
    private int type;			// 类型
    private int is_delicate;	// 是否精品
    private int is_not_price;	// 是否免费
    private int orderType;		// 排序条件(1=上传时间倒叙,2=观看数量倒叙,3=评论数量倒叙，)默认0=id倒叙（效果和上传时间倒叙一样），
    private String orderStr;       // 排序方式字符串 

    public String getOrderStr() {
        String str = new String []{"id asc ", "create_time desc", "see_count desc", "comment_count desc"}[orderType];
        return orderStr = "order by " + str ;
    }





}
