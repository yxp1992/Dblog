package com.zyd.blog.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 浓缩的user实体类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    // 主键
    private Long id;

    private String username;    // 用户昵称
    private String avatar;      // 用户头像地址
    private String sentence;    // 一句话自我介绍







}
