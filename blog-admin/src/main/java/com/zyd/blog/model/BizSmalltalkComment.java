package com.zyd.blog.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 消息通知表实体类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BizSmalltalkComment implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;    // 主键
    private String content;       // 内容
    private long create_uid;    // 消息产生人
    private long smalltalk_id;        // 所属说说id
    // private long reply_userid;        // 所回复的userid,值0代表无回复，直接评论的说说
    // private String reply_username;      // 如果reply_userid不为0,此值代表回复的username
    private Date create_time;   // 创建日期


    // 额外字段

    private String create_username;    //  消息产生人的username
    private String avatar;      // 头像地址



}
