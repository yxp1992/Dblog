package com.zyd.blog.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 权限表
 */
@Data
public class SysJur implements Serializable {

    private long id;
    private String jur_name;    // 权限名称
    private String jur_info;    // 权限详细介绍
    private String jur_warning; // 对无权者显示的警告


}
