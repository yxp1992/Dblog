package com.zyd.blog.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 申请回复表 -- end
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BizApply implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;            // 主键
    private String content;       // 申请内容
    private long create_uid;    // 消息产生人
    private long to_uid;    // 消息待处理人
    private int status;     // 消息状态(1=未处理,2=已同意,3=已拒绝)
    private String hand_content;    //  处理内容
    private Date create_time;   // 创建日期
    private Date update_time;   // 更新日期


    // 额外字段
    private String realname;        // 申请人真实姓名
    private String hand_realname;   // 待处理人真实姓名
    private String dept_name;   // 申请人所在部门


}
