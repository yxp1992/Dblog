package com.zyd.blog.webutil;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * session缓存集合工具类
 */
public class SessionListUtil {


    public static String pre = "list_";

    /**
     * 获取指定session指定key的Integer集合
     */
    public static List<Long> keyListGet(HttpSession session, String key) {
        if (session.getAttribute(key) == null) {
            session.setAttribute(key, new ArrayList<Integer>());
        }
        @SuppressWarnings("unchecked")
        List<Long> list = (List<Long>) session.getAttribute(key);
        return list;
    }


    /**
     * 指定key是否存在id
     */
    public static List<Long> keyListGet(HttpSession session, String key, long id) {



        List<Long> list = (List<Long>) session.getAttribute(key);
        return list;
    }



}
