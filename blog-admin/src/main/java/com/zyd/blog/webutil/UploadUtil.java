package com.zyd.blog.webutil;

import java.io.File;

import org.springframework.web.multipart.MultipartFile;

import com.zyd.blog.utils.RedJson;
import com.zyd.blog.utils.SysCfg;

/**
 * 文件上传工具类
 */
public class UploadUtil {

	

	// 上传文件 
	// 验证文件大小，文件类型  图片=image,视频=video
	public static RedJson savefile(MultipartFile file, String filePath, String ckType) {

        long size = file.getSize(); // 文件大小(B)
        if (size > SysCfg.up_max_size) {
            return RedJson.getJsonError("文件大小超过限制,上限1024M");
        }

        String currFileType = file.getContentType();	// 图片内部
        //System.out.println("文件类型：" + currFileType);
        if(currFileType.indexOf(ckType) != 0){
        	return RedJson.getJsonError("不符合规定的文件类型："+currFileType);
        }
        
        try {
            File outFile = new File(filePath);	// 本地磁盘位置
            file.transferTo(outFile);		// 转存文件
            return RedJson.getJsonSuccess("ok");	// 图片
        } catch (Exception e) {
            e.printStackTrace();
        	return RedJson.getJsonError(e.getMessage());  // 出现异常
        }
        
    }
	
	
}
