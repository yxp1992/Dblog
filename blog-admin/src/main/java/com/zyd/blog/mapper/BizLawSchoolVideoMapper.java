package com.zyd.blog.mapper;


import com.zyd.blog.model.BizLawSchoolVideo;
import com.zyd.blog.model.so.BizLawSchoolVideoSO;


import java.util.List;

/**
 * 法学院视频表 Mapper 接口
 */
public interface BizLawSchoolVideoMapper{


    
    /**
     * 保存一条
     */
    public int save(BizLawSchoolVideo bizLawSchoolVideo);


	
    /**
     * 根据ID获取
     */
    public BizLawSchoolVideo getById(Long id);

    
    /**
     * 查询，根据参数条件
     */
    public List<BizLawSchoolVideo> getList(BizLawSchoolVideoSO bizLawSchoolVideoSO);

   




}
