package com.zyd.blog.mapper;

import com.zyd.blog.model.SysJur;
import com.zyd.blog.model.SysRole;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色相关列表
 */
@Repository
public interface SysRoleMapper {

    // 写如一条
    public int save(SysRole sysRole);

    // 修改一条
    public int update(SysRole sysRole);


    // 查询所有角色
    public List<SysRole> getList();






}
