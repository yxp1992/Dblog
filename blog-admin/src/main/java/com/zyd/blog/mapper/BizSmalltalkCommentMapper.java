package com.zyd.blog.mapper;

import com.zyd.blog.model.BizSmalltalkComment;
import com.zyd.blog.utils.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 消息通知表Mapper
 */
public interface BizSmalltalkCommentMapper {



    /**
     * 根据ID获取
     */
    public BizSmalltalkComment getById(Long id);

    // 插入一条 #{content}, #{smalltalk_id}, #{create_uid}
    public int save(BizSmalltalkComment bizSmalltalkComment);


    // 拉取指定说说id的评论
    public List<BizSmalltalkComment> getList(
            @Param("sid") long sid,     // 说说id
            @Param("orderType") int orderType   // 排序方式（1=发表时间正序,2=发表时间倒叙）
    );



}
