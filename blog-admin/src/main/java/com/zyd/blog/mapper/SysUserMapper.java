/**
 * MIT License
 *
 * Copyright (c) 2018 yadong.zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.zyd.blog.mapper;

import com.zyd.blog.model.SysUser;
import com.zyd.blog.model.SysUserInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 用户Mapper接口
 */
@Repository
public interface SysUserMapper{


    // 修改个人基本资料
    public int updateInfo(SysUser sysUser);


    // 根据 Id 获取
    public SysUser getById(long id);

    // 根据 username 获取
    public SysUser getByUsername(String username);

    // 指定id成功登陆一次
    public int successLogin(@Param("id") long id, @Param("login_ip") String login_ip);


    // 返回指定id的各种count数据
    // apply_count=申请总数，reply_count=回复总数，follow_count=关注总数，fans_count=粉丝总数，release_count=发布说说总数
    public Map getCountById(long id);



    // 获取指定id的关注
    public List<SysUserInfo> getFollowUser(@Param("id") long id, @Param("is_fans") int is_fans);

    // 指定id的粉丝
    public List<SysUserInfo> getFansUser(long id);



}
