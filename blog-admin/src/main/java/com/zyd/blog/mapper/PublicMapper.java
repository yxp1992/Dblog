package com.zyd.blog.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * 定义常用的公共方法
 */
public interface PublicMapper {



    /**
     * 返回上一句SQL插入的自增主键值
     */
    public long getPkId();



    /**
     * 删除一条记录，指定表名，指定where条件
     */
    public int deleteById(
            @Param("tableName") String tName,   // 表名
            @Param("whereName") String wName,   // 条件字段名
            @Param("whereValue") Object wValue  //条件字段值
    );


    /**
     * 指定表的指定字段加X（可以<0 ）,根据指定条件(whereName=whereValue)
     */
    public int addX(
            @Param("tableName") String tName,
            @Param("columnName") String cName,
            @Param("x") int x,
            @Param("whereName") String wName,
            @Param("whereValue") Object wValue
        );

    /**
     * 指定表的指定字段更新为指定值,根据指定条件(whereName=whereValue)
     */
    public int updateColumn(
            @Param("tableName") String tName,
            @Param("columnName") String cName,
            @Param("value") Object value,
            @Param("whereName") String wName,
            @Param("whereValue") Object wValue
    );


    /**
     * 获取指定表的指定字段值,根据指定条件(whereName=whereValue)
     */
    public String getColumn(
            @Param("tableName") String tName,
            @Param("columnName") String cName,
            @Param("whereName") String wName,
            @Param("whereValue") Object wValue
    );


}
