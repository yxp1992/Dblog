package com.zyd.blog.mapper;

import com.zyd.blog.model.SysLoopPlay;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 轮播图管理表 Mapper 接口
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-03
 */
public interface SysLoopPlayMapper{

	
	/**
	 * 写入一条
	 */
	public int save(SysLoopPlay sysLoopPlay);

    /**
     * 删除一条，根据主键
     */
    public int deleteById(Long id);

    /**
     * 修改一条记录(标题、图片地址、超链接跳转地址、条件id)
     */
    public int update(SysLoopPlay sysLoopPlay);

    /**
     * 查询,根据条件
     * @param type 类型(1=首页顶部的,2=……)
     * @param is_enable 是否启用
//     * @param startDate 开始日期
//     * @param endDate 结束日期
//     * @param pageSize 页大小
     * @return
     */
    public List<SysLoopPlay> getList(
            @Param("type") int type,
            @Param("is_enable") int is_enable
    );







}
