package com.zyd.blog.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * 用户关系表数据
 */
public interface ManyUserUserMapper {



    // 写入关注
    public int save(@Param("uid") long uid, @Param("to_uid") long to_uid);

    // 删除关注
    public int delete(@Param("uid") long uid, @Param("to_uid") long to_uid);

    // 是否有此关注消息
    public int getByUidUid(@Param("uid") long uid, @Param("to_uid") long to_uid);



}
