package com.zyd.blog.mapper;

import com.zyd.blog.model.BizLawLib;
import com.zyd.blog.model.SysMessage;


/**
 * 消息通知表Mapper
 */
public interface SysMessageMapper{



    /**
     * 根据ID获取
     */
    public BizLawLib getById(Long id);

    // 插入一条 #{create_uid}, #{to_uid}, #{content}, #{goto_id}, #{type}
    public int save(SysMessage sysMessage);


    // 获取某人的全部通知



}
