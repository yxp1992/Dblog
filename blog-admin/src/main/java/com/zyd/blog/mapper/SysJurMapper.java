package com.zyd.blog.mapper;

import com.zyd.blog.model.SysJur;

import java.util.List;

/**
 * 权限表 Mapper
 */
public interface SysJurMapper {

    /**
     * 根据id查询
     */
    public SysJur getById(int id);

    /**
     * 查询列表，根据role_id
     */
    public List<SysJur> getList(int role_id);


    /**
     * 查询列表，int形式，根据role_id
     */
    public List<Integer> getListInt(int role_id);


    /**
     * 添加权限角色权限关系
     */
    public int saveRoleJur(int role_id, int jur_id);





}
