package com.zyd.blog.mapper;

import com.zyd.blog.model.BizLawLib;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 法库 Mapper接口 -- end
 */
public interface BizLawLibMapper{


    /**
     * 返回上一句SQL插入的自增主键值
     */
    public Long getPkId();

    /**
     * 根据类型获取
     * @param type 类型
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @param pageSize 页大小
     * @return
     */
    public List<BizLawLib> getListByType(
            @Param("type") Long type,
            @Param("startDate")String startDate,
            @Param("endDate")String endDate,
            @Param("pageSize")int pageSize
    );


    /**
     * 根据ID获取
     */
    public BizLawLib getById(Long id);


    /**
     * 删除一条，根据主键
     */
    public int deleteById(Long id);


}
