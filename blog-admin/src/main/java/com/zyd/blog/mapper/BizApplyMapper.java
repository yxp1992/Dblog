package com.zyd.blog.mapper;


import com.zyd.blog.model.BizApply;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  申请与回复 --Mapper 接口
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-06-27
 */
public interface BizApplyMapper {


    // 插入一个#{content}, #{create_uid}, #{to_uid}
    public int save(BizApply bizApply);

    // 查询，根据id
    public BizApply getById(Long id);


    /**
     * 处理一条申请,修改(status、hand_content、和时间)
     */
    public int handApply(BizApply bizApply);


    /**
     * 查询
     * @param userId 指定用户的数据
     * @param way 方式(1=获取我的申请 ,2=获取我的回复)
     * @param is_hand 是否已经处理 （1=是，2=否）
     * @return
     */
    public List<BizApply>  getByWayAndStatus(
            @Param("userId") Long userId,
            @Param("way") int way,
            @Param("is_hand") int is_hand
    );


}
