package com.zyd.blog.mapper;

import com.zyd.blog.model.BizLawSchoolComment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 法学院视频评论表 Mapper 接口
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-02
 */
public interface BizLawSchoolCommentMapper{

    /**
     * 插入一条
     */
    public int save(BizLawSchoolComment bizLawSchoolComment);


    /**
     * 删除一条，根据主键
     */
    public int deleteById(Long id);


    /**
     * 查询,根据条件
     * @param video_id 所属视频ID
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @param pageSize 页大小
     * @return
     */
    public List<BizLawSchoolComment> getListByCondition(
            @Param("video_id") Long video_id,
            @Param("startDate") String startDate,
            @Param("endDate") String endDate,
            @Param("pageSize") int pageSize
        );





}
