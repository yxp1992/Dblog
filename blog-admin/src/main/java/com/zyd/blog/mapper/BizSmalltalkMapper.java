package com.zyd.blog.mapper;

import com.zyd.blog.model.BizSmalltalk;
import com.zyd.blog.model.so.BizSmalltalkSO;
import com.zyd.blog.utils.PageAndData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  说说-Mapper 接口
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-06-20
 */
public interface BizSmalltalkMapper{

    /**
     * 插入一条数据,提供[user_id, content, keywords, original_id, img_list]
     */
    public int save(BizSmalltalk bizSmalltalk);

    /**
     * 获取一条数据
     */
    public BizSmalltalk getById(Long id);


    // 获取列表，根据指定条件
    public List<BizSmalltalk>getList(BizSmalltalkSO bizSmalltalkSO);


    // 获取指定id说说的转发列表
    public List<BizSmalltalk>getForwardList(long id);











}
