package com.zyd.blog.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 错误页总配置的地方
 */
@Controller
public class ErrorControllerA {



    // 转到admin端 相应错误页
    @RequestMapping("/admin/error/{code}")
    public String error(@PathVariable int code){
        return "/admin/error/" + code;
    }


}
