package com.zyd.blog.controller.admin;

import com.zyd.blog.webutil.JurUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 后台跳页面专用
 */
@RequestMapping("/admin/")
@Controller
public class AllControllerA {




    // ========================  默认的  =========================
    @RequestMapping({ "acc/*", "law_school/*", "loopplay/*"})
    public String gotoPage(HttpServletRequest request){
        return request.getRequestURI();
    }


    // ========================  重写的  =========================

    // 注销并跳转到登陆页
    @RequestMapping("acc/exit")
    public String acc_exit(HttpServletRequest request, HttpServletResponse response){
        JurUtil.removeUid(request, response);
        return "/admin/acc/login";
    }





}
