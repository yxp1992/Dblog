package com.zyd.blog.controller.api;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.zyd.blog.model.SysLoopPlay;
import com.zyd.blog.service.SysLoopPlayService;
import com.zyd.blog.utils.FS;
import com.zyd.blog.utils.PageAndData;
import com.zyd.blog.utils.RedJson;

import lombok.extern.slf4j.Slf4j;

/**
 * 轮播图管理表 前端控制器
 */
@Slf4j
@RestController
@RequestMapping("/sysLoopPlay/")
public class SysLoopPlayController {

    @Autowired
    SysLoopPlayService sysLoopPlayService;

    /**
     * 添加一条
     * 需要字段：
     * title：标题
     * imgSrc：图片地址
     * aHref：点击后跳转的超链接
     * isEnable：是否启用(1=是,2=否)
     * type：轮播图位置(1=首页顶部的,2=……)
     */
    @RequestMapping("add")
    public RedJson add(SysLoopPlay sysLoopPlay, HttpServletRequest request){
    	return RedJson.getJsonSuccess("ok", FS.sysLoopPlayService.add(sysLoopPlay));
    }


    /**
     * 删除一条，根据主键
     */
    @RequestMapping("deleteById")
    public RedJson deleteById(Long id, HttpServletRequest request){
    	return RedJson.getJsonByLine(FS.sysLoopPlayService.deleteById(id));
    }

    /**
     * 修改一条记录(标题、图片地址、超链接跳转地址、条件id)
     * 需要字段：
     * title：标题
     * imgSrc：图片地址
     * aHref：点击后跳转的超链接
     * isEnable：是否启用(1=是,2=否)
     * id：条件Id
     */
    @RequestMapping("update")
    public RedJson ResponseBean(SysLoopPlay sysLoopPlay, HttpServletRequest request){
    	return RedJson.getJsonByLine(FS.sysLoopPlayService.update(sysLoopPlay));
    }

    /**
     * 查询,根据条件
     * @param type 类型(1=首页顶部的,2=……) 默认1
     * @param is_enable 是否启用(1=是,2=否) 默认1
     * @return
     */
    @RequestMapping("getList")
    public RedJson getList(
    		@RequestParam(defaultValue = "1")int pageNo,
    		@RequestParam(defaultValue = "100")int pageSize,
    		@RequestParam(defaultValue = "1")int type, 
    		@RequestParam(defaultValue = "1")int is_enable
    		){
    	PageAndData pageAndData = FS.sysLoopPlayService.getList(pageNo, pageSize, type, is_enable);
        return new RedJson(200,"成功", pageAndData);
    }


    @InitBinder
    private void initBinder(WebDataBinder binder){
        binder.setDisallowedFields("create_time");//由表单到JavaBean赋值过程中哪一个值不进行赋值
    }



}

