package com.zyd.blog.controller.api;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zyd.blog.utils.NbUtil;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.utils.SysCfg;
import com.zyd.blog.webutil.UploadUtil;

/**
 * 文件上传
 */
@RestController
@RequestMapping("/upload/")
public class UploadController {
	
	
	
	
	// 上传图片
	@RequestMapping("img")
	public RedJson img(MultipartFile file, HttpServletRequest request){

		// 重命名文件
        String ext = NbUtil.getSuffixName(file.getOriginalFilename());
        String fileName  = NbUtil.getMarking28() + "." + ext;
		String filepath = SysCfg.up_abs_path + SysCfg.up_http_dyc + SysCfg.up_img_path;
		
		RedJson redJson = UploadUtil.savefile(file, filepath + fileName, "image");
		if(redJson.code == 200){
			redJson.setData(SysCfg.pathHttp + SysCfg.up_http_dyc + SysCfg.up_img_path + fileName);	// 返回给客户端的路径
		}
		return redJson;
	}
	
	
	
	// 上传视频
	@RequestMapping("video")
	public RedJson video(MultipartFile file, HttpServletRequest request){
		
		// 重命名文件
        String ext = NbUtil.getSuffixName(file.getOriginalFilename());
        String fileName  = NbUtil.getMarking28() + "." + ext;
		String filepath = SysCfg.up_abs_path + SysCfg.up_http_dyc + SysCfg.up_video_path;
		
		RedJson redJson = UploadUtil.savefile(file, filepath + fileName, "video");
		if(redJson.code == 200){
			redJson.setData(SysCfg.pathHttp + SysCfg.up_http_dyc + SysCfg.up_video_path + fileName);	// 返回给客户端的路径
		}
		return redJson;
	}
		
		
	
	// 图片

    // 获取某一帧为视频封面，并返回地址
    @RequestMapping("takeCover")
    public RedJson takeCover(String video_url, HttpServletRequest request){
        try {
            //http://localhost:8085/dycfile/video_upload/153127242349596798718.mp4
            int index = video_url.indexOf(SysCfg.up_http_dyc);
            String videoPath = SysCfg.up_abs_path + video_url.substring(index);

            String imgName  = NbUtil.getMarking28() + ".jpg";   // 封面名字
            String imgFilePath = SysCfg.up_abs_path + SysCfg.up_http_dyc + SysCfg.up_img_path + imgName;        // 封面本地磁盘地址
            String imgHttpPath = SysCfg.pathHttp + SysCfg.up_http_dyc + SysCfg.up_img_path + imgName;    // 返回给客户端的http地址
            fetchFrame(videoPath, imgFilePath);     // 开始提取
            return new RedJson(200, "提取成功", imgHttpPath);
        } catch (Exception e) {
            e.printStackTrace();
            return new RedJson(500, "提取失败", null);
        }
    }


    /**
     * 获取指定视频的帧并保存为图片至指定目录
     * @param videofile  源视频文件路径
     * @param framefile  截取帧的图片存放路径
     * @throws Exception
     */
    public static void fetchFrame(String videofile, String framefile)
            throws Exception {
        long start = System.currentTimeMillis();
        File targetFile = new File(framefile);
        FFmpegFrameGrabber ff = new FFmpegFrameGrabber(videofile);
        ff.start();
        int lenght = ff.getLengthInFrames();
        int i = 0;
        Frame f = null;
        while (i < lenght) {
            // 过滤前x帧，避免出现全黑的图片，依自己情况而定
            f = ff.grabFrame();
            if ((i > 100) && (f.image != null)) {
                break;
            }
            i++;
        }
        opencv_core.IplImage img = f.image;
        int owidth = img.width();
        int oheight = img.height();
        // 对截取的帧进行等比例缩放
        int width = 800;
        int height = (int) (((double) width / owidth) * oheight);
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        bi.getGraphics().drawImage(f.image.getBufferedImage().getScaledInstance(width, height, Image.SCALE_SMOOTH),
                0, 0, null);
        ImageIO.write(bi, "jpg", targetFile);
        //ff.flush();
        ff.stop();
        System.out.println(System.currentTimeMillis() - start);
    }



}
