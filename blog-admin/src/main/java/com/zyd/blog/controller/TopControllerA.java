package com.zyd.blog.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zyd.blog.utils.NbUtil;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.utils.SysCfg;
import com.zyd.blog.webutil.JurUtil;
import com.zyd.blog.webutil.RoleException;
import com.zyd.blog.webutil.WebUtil;

import java.io.File;
import java.io.IOException;

import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;


/**
 * 加强版控制器
 * 
 * @author kongyongshun
 *
 */
@ControllerAdvice	// 可指定包前缀(basePackages = "com.zyd.blog.controller.admin")
public class TopControllerA {

	private List<String> whiteList = Arrays.asList("/acc/doLogin", "/admin/acc/login");

	// 之前
	@ModelAttribute
	public void get(HttpServletRequest request) throws IOException {

		// 首次启动需要做的事情
		if (request.getServletContext().getAttribute("firstStart") == null) {
			request.getServletContext().setAttribute("firstStart", false);
			firstStart(request);
		}

		String str = "d-> " + NbUtil.getNow() +
				"\t\tIP: " + WebUtil.getRemoteHost(request) +
				"\tr-> " + request.getRequestURI() + 
				"\tp-> " + WebUtil.getParamsMap(request);
		System.err.println(str);

		if(JurUtil.getUid(request) == 0 && whiteList.contains(request.getRequestURI()) == false){
			throw new RoleException(999, "未登录");
		}

	}

	// 全局异常拦截
	@ExceptionHandler
	public void handlerException(Exception e, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		System.out.println("全局异常");
		e.printStackTrace();

		// 是否为ajax
		if(WebUtil.isAjax(request) == true){
			// ajax请求
			response.setContentType("application/json; charset=utf-8");	// http说明，我要返回JSON对象
			if (e instanceof RoleException == true) {		// 是否是权限异常
				RoleException re = (RoleException) e;
				System.out.println(JurUtil.getUid(request) + "企图通过非法权限(ajax)：" + re.getMessage());
				response.getWriter().print(RedJson.getJsonStr(re.getType(), re.getMessage()));	// 权限异常输出：e.type + 异常信息
			}else{
				response.getWriter().print(RedJson.getJsonStr(500, e.getMessage()));	// 普通异常输出：500 + 异常信息
			}
		}else{
			// 非ajax请求
			if (e instanceof RoleException == true) {		// 是否是权限异常
				RoleException re = (RoleException) e;
				System.out.println(JurUtil.getUid(request) + "企图通过非法权限：" + re.getMessage());
				if(re.getType() == 999){
					response.sendRedirect("/admin/acc/login");	// 未登录权限转到登录页
					return;
				}
			}
			throw e;
		}

	}




	/**
	 * 首次启动要做的事
	 */
	private static boolean firstStart(HttpServletRequest request) {

		System.out.println("首次启动-->");
		ServletContext context = request.getServletContext();
		String path = context.getContextPath();
		context.setAttribute("fpath", path);

		// 更新http上下文路径

		if (path.equals("/")) {
			// 无上下文
			StringBuffer url = request.getRequestURL();
			SysCfg.pathHttp = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/")
					.toString();
		} else {
			// 带上下文
			StringBuffer url = request.getRequestURL();
			SysCfg.pathHttp = url.delete(url.length() - request.getRequestURI().length(), url.length())
					.append(request.getServletContext().getContextPath()).append("/").toString();
		}
		System.out.println("http服务器地址：" + SysCfg.pathHttp);

		return true;
	}

}
