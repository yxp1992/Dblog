package com.zyd.blog.controller.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.zyd.blog.utils.SysCfg;

import java.io.File;
import java.nio.file.Paths;

/**
 * Springboot接管的动态资源目录
 */
@RestController
@RequestMapping("/" + SysCfg.up_http_dyc)
public class DycFileController {

    @Autowired
    private ResourceLoader resourceLoader;



    // springboot接管静态目录
    @RequestMapping(method = RequestMethod.GET, value = "/{fileway}/{filename:.+}")
    @ResponseBody
    public ResponseEntity<?> getFile(@PathVariable String fileway, @PathVariable String filename) {

        try {
            String filePath = Paths.get(SysCfg.up_abs_path + SysCfg.up_http_dyc , fileway, filename).toString();
            System.out.println("请求文件的地址-->：" + filePath);
            return ResponseEntity.ok(resourceLoader.getResource("file:" + filePath));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


}
