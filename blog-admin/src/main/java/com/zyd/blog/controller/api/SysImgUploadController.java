package com.zyd.blog.controller.api;


import com.zyd.blog.utils.RedJson;
import com.zyd.blog.webutil.JurUtil;
import lombok.extern.slf4j.Slf4j;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * <p>
 * 图片上传
 * </p>
 */
@Slf4j
@RestController
@RequestMapping("/sysImgUpload")
public class SysImgUploadController {

    @Value("${sysImgUpload.ROOT}")
    private String ROOT;    // 资源所在文件夹，相对路径(相对项目)
    @Value("${sysImgUpload.imgUploadPath}")
    private String imgUploadPath;    // 图片文件上传至所在文件夹
    @Value("${sysImgUpload.videoUploadPath}")
    private String videoUploadPath;    // 视频文件上传至所在文件夹

    public static final String httpDyc ="dycfile/";    // 动态文件路由前缀
    public static String staticFolder = null;    // 资源所在文件夹，绝对路径,通过 ROOT变量 求出来
    private String httpPath = null;    // 返回给客户端的http域名路径,根据上下文求出

    @Value("${sysImgUpload.fileSizeMax}")
    private int fileSizeMax; // 文件最大大小,单位/B
    @Value("${sysImgUpload.imgSuffix}")
    private String imgSuffix;	// 图片文件允许的后缀结合
    @Value("${sysImgUpload.videoSuffix}")
    private String videoSuffix;	// 视频文件允许的后缀结合

    @Value("${sysImgUpload.frame}")
    public int frame;     // 视频提取封面时指定帧（太小的帧容易是全黑画面）




    /**
     * 上传总入口
     * @param file 文件流
     * @param way 方式(1=图片，2=视频) 默认1
     * @param request
     * @return
     */
    @RequestMapping("/up")
    public RedJson up(MultipartFile file, @RequestParam(defaultValue = "1") int way, HttpServletRequest request){
        if(staticFolder == null){
            staticFolder = new File(ROOT).getAbsolutePath()+"/";
        }

        String uploadPath = (way == 1) ? imgUploadPath : videoUploadPath;   // 上传至地址
        String checkSuffix =  (way == 1 ? imgSuffix : videoSuffix);     // 文件检验后缀

        try{
            // 重命名文件
            String ext = getSuffixName(file.getOriginalFilename());
            String fileName  = System.currentTimeMillis()+"" + new Random().nextInt(Integer.MAX_VALUE)+"."+ext;

            saveImg(file, staticFolder + uploadPath, fileName, checkSuffix);
            //System.out.println("返回的http路径：" + getHttpPath(request)+uploadPath+fileName);
            log.info(JurUtil.getUid(request) + "上传了文件：" + staticFolder + uploadPath + fileName);
            return new RedJson(200,"正常",getHttpPath(request)+httpDyc+uploadPath+fileName); // 返回路径
        }catch (Exception e){
            return new RedJson(500,"上传失败",e.getMessage());
        }
    }

    /**
     * 上传图片总入口
     *
     * @param file 图片流
     * @param filePath 地址
     * @param fileName 文件名
     * @param checkSuffix 所要验证的后缀集合
     * @return 1=成功(地址),-1=失败(失败原因)
     */
    public boolean saveImg(MultipartFile file, String filePath, String fileName, String checkSuffix) {

        long size = file.getSize(); // 文件大小(B)
        if (size > fileSizeMax) {
            throw new RuntimeException("文件大小超过限制");
        }

        String ext = getSuffixName(filePath + fileName);// 后缀名

        List<String> tys = Arrays.asList(checkSuffix.split(","));// 所要判断的集合
        if (tys.contains(ext) == false) {
            throw new RuntimeException("文件后缀名格式错误！");
        }

        try {
            File exFile = new File(filePath);
            if(exFile.exists() == false){
                exFile.mkdirs();
            }

            log.info("文件存储至：" + filePath + fileName);
            File outFile = new File(filePath + fileName);	// 本地磁盘位置
            file.transferTo(outFile);	// 转存文件
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);  // 出现异常
        }
    }

    // 取文件后缀
    public static String getSuffixName(String fileName){
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }


    // 获得http域名路径
    public String getHttpPath(HttpServletRequest request){
        if(httpPath == null){
            String path = request.getServletContext().getContextPath();

            if (path.equals("/")) {
                // 无上下文
                StringBuffer url = request.getRequestURL();
                httpPath = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/")
                        .toString();
            } else {
                // 带上下文
                StringBuffer url = request.getRequestURL();
                httpPath = url.delete(url.length() - request.getRequestURI().length(), url.length())
                        .append(request.getServletContext().getContextPath()).append("/").toString();
            }
            log.info("http服务器地址初始化：" + httpPath);
        }
        return httpPath;
    }





    // =======================================================================================================




}

