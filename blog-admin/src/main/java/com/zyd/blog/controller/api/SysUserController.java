package com.zyd.blog.controller.api;

import com.zyd.blog.model.SysUser;
import com.zyd.blog.utils.*;
import com.zyd.blog.webutil.JurUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


/**
 * 用户表 API接口
 */
@RestController
@RequestMapping("/user/")
public class SysUserController {


    // 修改个人基本资料
    //    username 		账号昵称
    //    realname 		真实姓名
    //    avatar		头像地址
    //    sex           性别
    //    sentence 		一句话自我介绍
    //    mobile		手机号
    //    email			邮箱地址
    //    qq 			QQ号
    //    birthday 		出生日期
    //    address 		个人住址
    @RequestMapping("updateInfo")
    public RedJson updateInfo(SysUser sysUser, HttpServletRequest request){
        sysUser.setId(JurUtil.getUid(request));
        return RedJson.getJsonByLine(FS.sysUserService.updateInfo(sysUser));
    }

    // 1、根据id查询信息
    @RequestMapping("getInfoById")
    public RedJson getInfoById(long id){
        SysUser sysUser = FS.sysUserService.getById(id);
        sysUser.setPassword("********");
        return RedJson.getJsonSuccess("ok").setData(sysUser);
    }

    // 2、获取当前登陆的user信息
    @RequestMapping("getInfoCurr")
    public RedJson getInfoCurr(HttpServletRequest request){
        return getInfoById(JurUtil.getUid(request));
    }


    // 3、返回指定id的各种count数据
    @RequestMapping("getCountById")
    public RedJson getCountById(long id){
        return RedJson.getJsonSuccess("ok", FS.sysUserService.getCountById(id));
    }

    // 4、返回指定id的各种count数据
    @RequestMapping("getCountCurr")
    public RedJson getCountCurr(HttpServletRequest request){
        return getCountById(JurUtil.getUid(request));
    }

    // 5、判断我是否关注了指定id
    @RequestMapping("isFollow")
    public RedJson isFollow(long id, HttpServletRequest request){
        long uid = JurUtil.getUid(request);
        return RedJson.getJsonSuccess("ok", FS.manyUserUserService.getByUidUid(uid, id) > 0);
    }

    // 6、关注指定id
    @RequestMapping("doFollow")
    public RedJson doFollow(long id, HttpServletRequest request){
        long uid = JurUtil.getUid(request);
        if(uid == id){
            return RedJson.getJsonError("不能自己关注自己");
        }
        if(FS.manyUserUserService.getByUidUid(uid, id) > 0){
            return RedJson.getJsonError("已经关注他了，不能重复关注");
        }
        return RedJson.getJsonSuccess("ok", FS.manyUserUserService.save(uid, id) > 0);
    }

    // 7、取消关注指定id
    @RequestMapping("unFollow")
    public RedJson unFollow(long id, HttpServletRequest request){
        long uid = JurUtil.getUid(request);
        return RedJson.getJsonSuccess("ok", FS.manyUserUserService.delete(uid, id) > 0);
    }

    // 8、拉取我的关注
    /**
     * 查询
     * @param is_fans 是否互关，（1=是，拉取我的互关，2=否，只拉取我的关注，默认值2）
     * @return
     */
    @RequestMapping("getFollowUser")
    public RedJson getFollowUser(
            @RequestParam(defaultValue = "1") int pageNo,
            @RequestParam(defaultValue = "10") int pageSize,
            @RequestParam(defaultValue = "2") int is_fans, //
            HttpServletRequest request
        ){
        long currUserId = JurUtil.getUid(request);
        PageAndData data = FS.sysUserService.getFollowUser(Page.getPage(pageNo, pageSize), currUserId, is_fans);
        return RedJson.getJson(200,"ok", data);
    }

    // 9、 拉取我的粉丝
    @RequestMapping("getFansUser")
    public RedJson getFansUser(
            @RequestParam(defaultValue = "1") int pageNo,
            @RequestParam(defaultValue = "10") int pageSize,
            HttpServletRequest request
    ){
        long currUserId = JurUtil.getUid(request);
        PageAndData data = FS.sysUserService.getFansUser(Page.getPage(pageNo, pageSize), currUserId);
        return RedJson.getJson(200,"ok", data);
    }






    /**
     * 获取当前登陆者的各种count信息
     * @retur
     */
//    @RequestMapping("/getStatistics")
//    public ResponseBean getStatistics(HttpServletRequest request){
//
//
//        Long userId = JWTUtil.getUserIdByReq(request);
//        SysUser sysUser = sysUserService.getByUserId(userId);
//        Map map = new HashMap();
////        map.put("applyCount",sysUser.getApplyCount());
////        map.put("fansCount",sysUser.getFansCount());
////        map.put("followCount",sysUser.getFollowCount());
////        map.put("msgCount",sysUser.getMsgCount());
////        map.put("replyCount",sysUser.getReplyCount());
//
//        return new ResponseBean(200,"成功",map);
//    }
//
//    /**
//     * 获取当前登陆者的账号基本信息
//     * @return
//     */
//    @RequestMapping("/getUserInfo")
//    public ResponseBean getUserInfo(HttpServletRequest request){
//        Long userId = JWTUtil.getUserIdByReq(request);
//        SysUser2 sysUser = sysUserService.getByUser2Id(userId);
//        Map map = new HashMap();
//        map.put("avatar",sysUser.getAvatar());
//        map.put("nickName",sysUser.getNickname());
//        map.put("userId",sysUser.getId());
//        map.put("userName",sysUser.getUsername());
//        map.put("realname",sysUser.getRealname());  // 真实姓名
//        map.put("dept_id", sysUser.getDeptId());   // 部门id
//        map.put("dept_name",sysUser.getDeptName());  // 部门名称
//
//        return new ResponseBean(200,"成功",map);
//    }




}
