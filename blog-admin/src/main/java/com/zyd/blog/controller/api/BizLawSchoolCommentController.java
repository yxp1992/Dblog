package com.zyd.blog.controller.api;


import com.zyd.blog.service.BizLawSchoolCommentService;
import com.zyd.blog.model.BizLawSchoolComment;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.webutil.JurUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-06-20
 */
@Slf4j
@RestController
@RequestMapping("/BizLawSchoolCommentController")
public class BizLawSchoolCommentController {

    @Autowired
    BizLawSchoolCommentService bizLawSchoolCommentService;

    /**
     * 增加一条评论
     * @param content 内容
     * @param video_id 所属视频id
     */
    @RequestMapping("/add")
    public RedJson add(String content, Long video_id, HttpServletRequest request){
        try{
            Long userId = JurUtil.getUid(request); // 当前登陆userid
            bizLawSchoolCommentService.add(content, video_id, userId);
            return new RedJson(200,"插入成功",null);
        }catch(Exception e){
            return new RedJson(500,"插入失败："+e.getMessage(),null);
        }
    }


    /**
     * 删除一条,根据id
     */
    @RequestMapping("/deleteById")
    public RedJson deleteById(Long id, HttpServletRequest request){
        try{
            Long userId = JurUtil.getUid(request);
            int a = bizLawSchoolCommentService.deleteById(id);
            if(a == 0){
                return new RedJson(500,"删除失败：指定的法学院视频评论不存在",null);
            }
            log.info("用户"+userId+"删除法学院视频评论："+id);
            return new RedJson(200,"删除成功",null);
        }catch(Exception e){
            return new RedJson(500,"删除失败："+e.getMessage(),null);
        }
    }


    /**
     * 查询,根据条件
     * @param video_id 所属视频ID
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @return
     */
    @RequestMapping("/getListByCondition")
    public RedJson getListByCondition(Long video_id, String startDate, String endDate){
        List<BizLawSchoolComment> data = bizLawSchoolCommentService.getListByCondition(video_id,startDate,endDate,10);
        return new RedJson(200,"成功", data);
    }



}

