package com.zyd.blog.controller.api;



import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zyd.blog.model.BizLawSchoolVideo;
import com.zyd.blog.model.so.BizLawSchoolVideoSO;
import com.zyd.blog.utils.FS;
import com.zyd.blog.utils.PageAndData;
import com.zyd.blog.utils.RedJson;



/**
 * 法学院视频表 API
 */
@RestController
@RequestMapping("/bizLawSchoolVideo/")
public class BizLawSchoolVideoController {


    // 根据Id获取
    @RequestMapping("getById")
    public RedJson getById(long id){
    	return RedJson.getJsonSuccess("ok", FS.bizLawSchoolVideoService.getById(id));
    }


	//    发布一条法库，需要提供：
	//    title, video_url, cover_url, type, price(价格), is_delicate(是否精品)
    @RequestMapping("doAdd")
    public RedJson doAdd(BizLawSchoolVideo bizLawSchoolVideo, HttpServletRequest request){
        Long pkid = FS.bizLawSchoolVideoService.save(bizLawSchoolVideo);
        return new RedJson(200,"插入成功",pkid);
    }

    // 删除一条
    @RequestMapping("deleteById")
    public RedJson deleteById(Long id,HttpServletRequest request){
    	int a = FS.bizLawSchoolVideoService.deleteById(id);
    	return RedJson.getJsonByLine(a);
    }



    // 根据参数条件查询
    @RequestMapping("getList")
    public RedJson getList(BizLawSchoolVideoSO so){
    	PageAndData pageAndData = FS.bizLawSchoolVideoService.getListByParam(so);
		return RedJson.getJsonSuccess("ok", pageAndData);
    }

    
    


}

