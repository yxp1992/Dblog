package com.zyd.blog.controller.api;


import com.zyd.blog.service.BizLawLibService;
import com.zyd.blog.model.BizLawLib;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.webutil.JurUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-06-29
 */
@Slf4j
@RestController
@RequestMapping("/bizLawLib")
public class BizLawLibController {

    @Autowired
    BizLawLibService bizLawLibService;

    // 根据类型查询法律法规库
    @RequestMapping("/getLawInfoByType")
    public RedJson getLawInfoByType(Long type, String startDate, String endDate){
        List<BizLawLib> data = bizLawLibService.getListByType(type,startDate,endDate);
        return new RedJson(200,"成功", data);
    }

    // 根据id查询法律法规库详情
    @RequestMapping("/getLawInfoById")
    public RedJson getLawInfoById(Long id){
        BizLawLib data= bizLawLibService.getById(id);
        return new RedJson(200,"成功", data);
    }

    /**
     * 发布一条法库
     */
    @RequestMapping("/sendLawLib")
    public RedJson sendLawLib(String title, String content, Long type, String cover_url, HttpServletRequest request){
        try{
            Long userId = JurUtil.getUid(request);
            Long pkid = bizLawLibService.addLawLib(title,content,type,cover_url,userId);
            return new RedJson(200,"插入成功",pkid);
        }catch(Exception e){
            return new RedJson(500,"插入失败："+e.getMessage(),null);
        }
    }

    /**
     * 删除一条
     */
    @RequestMapping("/deleteById")
    public RedJson deleteById(Long id,HttpServletRequest request){
        try{
            Long userId = JurUtil.getUid(request);
            int a = bizLawLibService.deleteById(id);
            if(a == 0){
                return new RedJson(500,"删除失败：指定法库id不存在",null);
            }
            log.info("用户"+userId+"删除法库："+id);
            return new RedJson(200,"删除成功",null);
        }catch(Exception e){
            return new RedJson(500,"删除失败："+e.getMessage(),null);
        }
    }



}

