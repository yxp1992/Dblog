package com.zyd.blog.controller.api;

import com.zyd.blog.model.BizSmalltalk;
import com.zyd.blog.model.BizSmalltalkComment;
import com.zyd.blog.model.so.BizSmalltalkSO;
import com.zyd.blog.utils.FS;
import com.zyd.blog.utils.Page;
import com.zyd.blog.utils.PageAndData;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.webutil.JurUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * N：说说-控制器
 */
@RestController
@RequestMapping("/smalltalk/")
public class SmalltalkController {



    // 原创或转发说说,返回 id 主键
    @RequestMapping("send")
    public RedJson send(
            String content,
            @RequestParam(defaultValue = "") String imgList,    // 图片地址集合
            @RequestParam(defaultValue = "") String videoList,    // 视频地址集合
            @RequestParam(defaultValue = "0") Long original_id,    // 要转发的原创说说id,值0代表为原创
            HttpServletRequest request){
        try{
            // [user_id, content, keywords, original_id, img_list]
            Long currUserId = JurUtil.getUid(request);
            BizSmalltalk bizSmalltalk = new BizSmalltalk();
            bizSmalltalk.setContent(content);   // 内容
            bizSmalltalk.setKeywords("");   // 先写死keywords
            bizSmalltalk.setUserId(currUserId); // 说说发表人
            //bizSmalltalk.setUsername(JWTUtil.getUserNameByReq(request));
            bizSmalltalk.setOriginalId(original_id);    // 原创说说id,值0代表为原创
            bizSmalltalk.setImgList(imgList);       // 说说附带图片集合
            bizSmalltalk.setVideoList(videoList);   // 附带的视频集合
            bizSmalltalk.setType(1);        // 说说被鉴别的类型(1=普通,2=图片,3=视频)
            if(imgList.length() > 2 ){
                bizSmalltalk.setType(2);
            }
            if(videoList.length() > 2){
                bizSmalltalk.setType(3);
            }
            Long pkid = FS.bizSmalltalkService.add(bizSmalltalk);
            return RedJson.getJson(200,"发表成功",pkid);
        }catch (Exception e){
            e.printStackTrace();
            return RedJson.getJson(500,"发表失败:"+e.getMessage(),null);
        }
    }

    // 根据id获取指定说说：
    @RequestMapping("getById")
    public RedJson getById(Long id){
        return RedJson.getJson(200, "ok", FS.bizSmalltalkService.getById(id));
    }

    // 说说删除
    @RequestMapping("deleteById")
    public RedJson deleteById(Long id, HttpServletRequest request){
        BizSmalltalk bizSmalltalk = FS.bizSmalltalkService.getById(id);
        if(bizSmalltalk == null){
            return RedJson.getJson(500, "指定的说说不存在", null);
        }
        if(bizSmalltalk.getUserId() != JurUtil.getUid(request)){
            return RedJson.getJson(500, "抱歉，您并非此说说发表人，无权删除", null);
        }
        int a = FS.bizSmalltalkService.delete(id);
        if(a < 1){
            return RedJson.getJson(500, "删除失败", null);
        }
        return RedJson.getJson(200,"ok",null);
        // return new ResponseBean()
    }




    // 说说点赞
    @RequestMapping("doLike")
    public RedJson doLike(long id, HttpServletRequest request){
        FS.bizSmalltalkService.doLike(id, JurUtil.getUid(request));
        return RedJson.getJson(200,"点赞成功", null);
    }

    // 说说取消点赞
    @RequestMapping("unLike")
    public RedJson unLike(long id, HttpServletRequest request){
        FS.bizSmalltalkService.unLike(id, JurUtil.getUid(request));
        return RedJson.getJson(200,"取消点赞成功", null);
    }


    // 获取指定说说，具体条件点进去SO类
    @RequestMapping("getList")
    public RedJson getList(BizSmalltalkSO so, HttpServletRequest request){
        PageAndData pageAndData = FS.bizSmalltalkService.getList(so);
        return RedJson.getJsonSuccess("ok", pageAndData);
    }


    // 说说热词列表获取（测试方法）
    @RequestMapping("getHotwords")
    public RedJson getHotwords(@RequestParam(defaultValue="10") int num){
    	String [] words = new String[]{"高检院", "泰安", "检察院", "浪潮", "哈哈", "万达", "腾讯", "阿里", "百度", "凉云科技"};
    	return RedJson.getJsonSuccess("ok", words);
    }

    // ==========================     说说评论部分      ================================

    // 说说评论
    @RequestMapping("addComment")
    public RedJson addComment(
            String content,     // 内容
            long smalltalk_id,  // 回复的说说id
            HttpServletRequest request){
        try{
            BizSmalltalkComment bizSmalltalkComment = new BizSmalltalkComment();
            bizSmalltalkComment.setContent(content);    //内容
            bizSmalltalkComment.setSmalltalk_id(smalltalk_id);  // 评论的说说id
            bizSmalltalkComment.setCreate_uid(JurUtil.getUid(request)); // 创建人id

            Long id = FS.bizSmalltalkService.addComment(bizSmalltalkComment);
            return RedJson.getJson(200,"评论成功", id);
        }catch (Exception e){
            e.printStackTrace();
            return RedJson.getJson(500,"评论失败:"+e.getMessage(),null);
        }
    }

    // 说说评论删除
    @RequestMapping("delComment")
    public RedJson delComment(long id){
        return RedJson.getJsonByLine(FS.bizSmalltalkService.delComment(id));
    }


    // 获取指定id说说评论列表
    @RequestMapping("getCommentList")
    public RedJson getCommentList(
            @RequestParam(defaultValue = "1") int pageNo,
            @RequestParam(defaultValue = "10") int pageSize,
            long smalltalk_id,    // 指定说说id
            @RequestParam(defaultValue = "1") int orderType    // 排序方式（1=发表时间正序,2=发表时间倒叙）默认=1
        ){
        PageAndData pageAndData = FS.bizSmalltalkService.getCommentList(Page.getPage(pageNo, pageSize), smalltalk_id, orderType);
        return RedJson.getJsonSuccess("ok", pageAndData);
    }

    // 获取指定id说说转发列表
    @RequestMapping("getForwardList")
    public RedJson getForwardList(
            @RequestParam(defaultValue = "1") int pageNo,
            @RequestParam(defaultValue = "10") int pageSize,
            long id
        ){
        PageAndData pageAndData = FS.bizSmalltalkService.getForwardList(pageNo, pageSize, id);
        return RedJson.getJsonSuccess("ok", pageAndData);
    }



}
