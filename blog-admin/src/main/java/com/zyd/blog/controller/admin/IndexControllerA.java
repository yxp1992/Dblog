package com.zyd.blog.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页控制器
 */
@Controller
public class IndexControllerA {


    // 跳转到后台首页
    @RequestMapping({"/", "/admin", "/admin/index"})
    public String index(){
        return "/admin/index";
    }

    // 跳转首页第一副面板
    @RequestMapping("/admin/first")
    public String first(){
        return "/admin/first";
    }




}
