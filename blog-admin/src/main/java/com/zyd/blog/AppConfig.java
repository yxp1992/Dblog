package com.zyd.blog;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 项目配置类
 *
 */
@Configuration
public class AppConfig extends WebMvcConfigurerAdapter {


    // 配置错误页
    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return new EmbeddedServletContainerCustomizer() {
            @Override
            public void customize(ConfigurableEmbeddedServletContainer container) {
                container.addErrorPages(
                        new ErrorPage(HttpStatus.BAD_REQUEST, "/admin/error/400"),
                        new ErrorPage(HttpStatus.UNAUTHORIZED, "/admin/error/401"),
                        new ErrorPage(HttpStatus.NOT_FOUND, "/admin/error/404"),
                        new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/admin/error/500")
                );
            }

        };
    }



	/**  
     * 文件上传配置  
     * @return  
     */  
//    @Bean
//    public MultipartConfigElement multipartConfigElement() {
//        MultipartConfigFactory factory = new MultipartConfigFactory();
//        //单个文件最大
//        factory.setMaxFileSize("500MB"); //KB,MB
//        /// 设置总上传数据总大小
//        factory.setMaxRequestSize("5000MB");
//        return factory.createMultipartConfig();
//    }
	
	
//	/**
//	 * 虚拟路径映射
//	 */
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {  
//		  //文件磁盘图片url 映射  
//		  //配置server虚拟路径，handler为前台访问的目录，locations为files相对应的本地路径  
//		  registry.addResourceHandler("/static/**").addResourceLocations("E:\\static\\");  
//    }  
//    
    
    
}
