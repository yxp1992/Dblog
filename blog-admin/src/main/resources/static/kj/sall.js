// 共用js代码库
(function () {
    /**
     * ajax的再次封装
     * @param url 请求地址
     * @param data 数据
     * @param success 成功的回调(code为200)
     * @param success2 成功的回调(code码为非200--一般为500)
     * @param errorfn 异常回调
     */
    $.ajax2=function(url, data, success, success2, errorfn) {
        var load=layer.msg('努力加载中……', {icon: 16,shade: 0.01,time:1000*10 });
        $.ajax(url, {
            type: 'get',
            data: data,
            dataType: 'text',
            success: function(res) {
                layer.close(load);
                res = JSON.parse(res);
                if(res.code == 200) {
                    success(res);
                } else {
                    if(success2){
                        return success2(res);
                    }
                    layer.alert("失败："+res.msg);
                }
            },
            error: function(xhr, type, errorThrown) {
                layer.close(load);
                if(errorfn){
                    return errorfn(xhr, type, errorThrown);
                }
                layer.alert(JSON.stringify(xhr));
            }
        });
    }


    var me={};
    window.$top = me;
})();
