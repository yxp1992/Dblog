var navs = [

{
	"title": "用户管理",
	"icon": "&#xe609;",
	"href": "###",
	"spread": false,
	"children": [
		{
			"title": "用户列表",
			"icon": "&#xe631;",
			"href": "page/news/news_get_list.html",
			"spread": false
		},
		{
			"title": "用户添加",
			"icon": "&#xe631;",
			"href": "page/news/news_add.html",
			"spread": false
		}
	]
}, 


{
	"title": "权限管理",
	"icon": "&#xe64c;",
	"href": "###",
	"spread": false,
	"children": [
		{
			"title": "权限列表",
			"icon": "&#xe631;",
			"href": "page/advert/advert_get_list.html",
			"spread": false
		},{
            "title": "资源列表",
            "icon": "&#xe631;",
            "href": "page/advert/advert_get_list.html",
            "spread": false
        },{
            "title": "角色列表",
            "icon": "&#xe631;",
            "href": "page/advert/advert_get_list.html",
            "spread": false
        },{
            "title": "权限分配",
            "icon": "&#xe631;",
            "href": "page/advert/advert_get_list.html",
            "spread": false
        }
	]
}, 

{
	"title": "说说管理",
	"icon": "&#xe60d;",
	"href": "###",
	"spread": false,
	"children": [{
			"title": "说说列表",
			"icon": "&#xe631;",
			"href": "page/card/brand_get_list.html",
			"spread": false
		},{
			"title": "说说发布",
			"icon": "&#xe631;",
			"href": "page/advert/advert_get_list.html",
			"spread": false
		}
	]
}, 


{
	"title": "文章管理",
	"icon": "&#xe612;",
	"href": "###",
	"spread": false,
	"children": [{
			"title": "文章列表",
			"icon": "&#xe631;",
			"href": "page/user/user_get_list.html",
			"spread": false
		},
		{
			"title": "文章添加",
			"icon": "&#xe631;",
			"href": "page/user/level_get_list.html",
			"spread": false
		}
	]
}, 


{
	"title": "统计报表",
	"icon": "&#xe629;",
	"href": "###",
	"spread": false,
	"children": [{
			"title": "日活统计",
			"icon": "&#xe631;",
			"href": "page/baotable/orders_top.html",
			"spread": false
		},{
			"title": "访问量统计",
			"icon": "&#xe631;",
			"href": "page/baotable/card_top.html",
			"spread": false
		},{
			"title": "用户统计",
			"icon": "&#xe631;",
			"href": "page/baotable/user_top.html",
			"spread": false
		}
	]
}, 

{
	"title": "系统维护",
	"icon": "&#xe614;",
	"href": "###",
	"spread": false,
	"children": [
	
		{
			"title": "系统参数",
			"icon": "&#xe631;",
			"href": "page/system/system_setup.html",
			"spread": false
		},
		{
			"title": "数据备份",
			"icon": "&#xe631;",
			"href": "page/system/dbback_get_list.html",
			"spread": false
		}
	]
},
{
	"title": "首页管理",
	"icon": "&#xe614;",
	"href": "###",
	"spread": false,
	"children": [
		{
			"title": "轮播图管理",
			"icon": "&#xe634;",
			"href": "loopplay/list",
			"spread": false
		}
	]
},
{
	"title": "法学院管理",
	"icon": "&#xe614;",
	"href": "###",
	"spread": false,
	"children": [

		{
			"title": "法学院列表",
			"icon": "&#xe60a;",
			"href": "page/system/system_setup.html",
			"spread": false
		},
		{
			"title": "法学院添加",
			"icon": "&#xe654;",
			"href": "/admin/law_school/add",
			"spread": false
		}
	]
}



]