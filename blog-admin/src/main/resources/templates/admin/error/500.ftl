<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>简诉404</title>
    <style type="text/css">
        *{padding: 0px;margin: 0px;}
        html{
            height: 100%;
            overflow: hidden;
        }
        body{
            background-color: #AAA;
            background-size: 100% 100%;
            background-image: url(/img/error/500.jpg);
        }
        .xiaoxi{
            color: #FFF;
            font-size: 14px;
            text-decoration: none;
            text-align: center;
            line-height: 40px;
            border-radius: 2px;
            border: 0px #CCC solid;
            background-color:rgba(255,255,255,0.2);
            width: 100px;
            height: 40px;
            position: absolute;
            top: 45%;
            left:50%;
            margin-top: -20px;
            margin-left: -50px;
        }
        .xiaoxi:hover{color: #DDD;}

    </style>
</head>
<body>
<a href="/admin/acc/login" class="xiaoxi" target="_top">返回首页</a>
</body>
</html>