<!DOCTYPE html>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <title>留言反馈列表</title>

        <!-- 所有资源 -->
        <#include "/admin/in/jslib.ftl"/>

        <style>
            .main-box{
                margin-top: 10px;
                min-height: 600px;
                border: 0px #ddd solid;
            }

            #ff .item-fox{margin: 1em 1em;}
            .layui-form-label{}
            #ff .layui-input{width: 300px;}
            #ff .layui-textarea{width: 400px;}


        </style>
	
	</head>
	<body>

		<div class="main-box">


			<!-- 正文开始 -->
			<form id="ff" class="layui-form" action="#" method="post">

				<div class="item-fox">
					<div><img style="vertical-align: top;" src="/assets/img/add.png"/> <span style="font-size: 16px;">法学院视频添加</span><hr/></div>
				</div>

				<br>
				<div class="item-fox">
					<label class="layui-form-label">标题：</label>
					<input class="layui-input" v-model="video.title" />
				</div>

				<div class="item-fox">
					<label class="layui-form-label">价格：</label>
					<input class="layui-input" v-model="video.price" type="number"  />
				</div>

				<div class="item-fox">
					<label class="layui-form-label">精品：</label>
					<input type="radio" name="is_delicate" value="1" title="是" v-model="video.is_delicate" />
					<input type="radio" name="is_delicate" value="0" title="否" v-model="video.is_delicate"  />
				</div>

				<div class="item-fox">
					<label class="layui-form-label">视频：</label>
					<div style="line-height: 40px;">
						<a style="color: #66f;" :href="video.video_url" target="_blank" v-if="video.video_url!=''">
							{{video.video_url == '' ? '暂未上传' : video.video_url}}
						</a>
						<a style="color: blue;cursor: pointer;" class="upload_video">&nbsp;点击上传</a>
					</div>
				</div>

				<div class="item-fox">
					<label class="layui-form-label">封面：</label>
					<div style="line-height: 40px;">
						<img :src="video.cover_url" style="width:50px;height:40px;cursor: pointer; " title="点击预览" @click="bigImg(video.cover_url)"  v-if="video.cover_url!=''"/>
						<a style="color: blue;cursor: pointer;" class="upload_img">&nbsp;点击上传</a>
						<a style="color: blue;cursor: pointer;" @click="takeCover()">&nbsp;从视频提取</a>
					</div>
				</div>
				

				<div class="item-fox">
					<label class="layui-form-label"></label>
					<input type="button" class="layui-btn layui-btn-small dkt-bg-two" value="确认" @click="add()"/>
				</div>

			</form>




		</div>

        <script>

            var app = new Vue({
                el: '.main-box',
                data: {
                    video: {
                        title: '',       //标题
                        cover_url: '',   // 封面地址  --'/assets/img/qxz.png',
                        video_url: '',    // 视频地址
                        price: 0,      // 价格
                        type: 1,        // 类型
                        is_delicate:0    // 是否精品(1=是,2=否)
                    }
                },
                methods: {
                    add: function () {
                        var video = this.video;
                        if(video.title == '' || video.cover_url=='' || video.video_url=='' ){
                            return layer.msg('请补全信息!');
                        }
                        video.is_delicate = document.querySelector('[name=is_delicate]:checked').value
                        $.ajax2('/bizLawSchoolVideo/doAdd',video,function(res){
                            layer.alert('添加成功：'+res.data);
                        })
                    },
                    takeCover: function () {
                        var video = this.video;
                        if(video.video_url == ''){
                            return layer.msg('尚未上传视频,请先上传！');
                        }
                        $.ajax2('/upload/takeCover',{video_url: video.video_url}, function (res) {
                            layer.msg('提取成功。');
                            video.cover_url = res.data;
                        })

                    },
                    bigImg: function (imgSrc) {
                        var content = '<img style="width:100%;height:100%;" src="'+imgSrc+'" />';
                        layer.open({
                            type: 1,
                            title: '图片预览',
                            shadeClose: true,
                            area: ['500px','500px'], //宽高
                            content: content
                        });
                    }
                },
                created: function () {
                    setTimeout(function () {
                        layui.form.render();
                        //更换图片
                        layui.upload.render({
                            elem: '.upload_img' 					//绑定元素
                            ,url: '/upload/img' 			//上传接口
                            ,before: function () {
                                layer.msg('努力加载中……', {icon: 16,shade: 0.01,time:1000*10 });
                            }
                            ,done: function(res,index){
                                if(res.code==200){
                                    app.video.cover_url = res.data;
                                    layer.msg('图片上传成功');
                                }else{
                                    layer.alert("失败："+JSON.stringify(res));
                                }
                            }
                            ,error: function(e,ee){
                                layer.alert("上传失败，异常："+e);
                            }
                        });

                        // 上传视频
                        layui.upload.render({
                            elem: '.upload_video' 					//绑定元素
                            ,url: '/upload/video' 			//上传接口
                            ,accept: 'video'
                            ,before: function () {
                                layer.msg('努力加载中……', {icon: 16,shade: 0.01,time:1000*10 });
                            }
                            ,done: function(res,index){
                                layer.closeAll();
                                if(res.code==200){
                                    app.video.video_url = res.data;
                                    layer.confirm('视频上传成功，是否从视频中提取帧作为封面？',{btn:['好的','不&nbsp;！']},function () {
                                        app.takeCover();
                                    })
                                }else{
                                    layer.alert("失败："+JSON.stringify(res));
                                }
                            }
                            ,error: function(e,ee){
                                layer.closeAll();
                                layer.alert("上传失败，异常："+e);
                            }
                        });


                    },1)
                }
            })






        </script>
	
	</body>
</html>