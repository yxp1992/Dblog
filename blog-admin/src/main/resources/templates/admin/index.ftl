<!--
	作者：985449475@qq.com
	时间：2018-04-01
	描述：已完成
-->
<!DOCTYPE html>
<html>

<head>
    <base href="/admin/index">
    <meta charset="utf-8">
    <title>简诉后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="favicon.ico">

    <link rel="stylesheet" href="/kj/layui/css/layui.css">
    <#--<link rel="stylesheet" href="layui/css/layui.css" media="all" />-->
    <link rel="stylesheet" href="/admin/css/font_eolqem241z66flxr.css" media="all" />
    <link rel="stylesheet" href="/admin/css/main.css" media="all" />

    <script type="text/javascript">
        setInterval(function() {
            var da = new Date();
            var Y = da.getFullYear(); //年
            var M = da.getMonth() + 1; //月
            var D = da.getDate(); //日
            var h = da.getHours(); //小时
            var sx = "凌晨";
            if(h >= 6) {
                sx = "上午"
            }
            if(h >= 12) {
                sx = "下午";
                h -= 12;
            }
            if(h >= 18) {
                sx = "晚上";
            }
            var m = da.getMinutes(); //分
            var s = da.getSeconds(); //秒
            var z = da.getDay(); //周几
            var zong = "";

            zong += Y + "-" + M + "-" + D + " " + sx + " " + h + ":" + m + ":" + s + " 周" + z;
            document.getElementById("shijian").innerHTML = zong;
        }, 1000);
    </script>

</head>

<body class="main_body">
    <div class="layui-layout layui-layout-admin">
        <!-- 顶部 -->

        <div class="layui-header header">

            <div class="layui-main">
                <a href="javascript:;" class="logo">简诉后台管理</a>

                <small class="logo">
                    <i class="layui-icon">&#xe60e;</i>
                    <small><small id="shijian"></small></small>
                </small>

                <!-- 顶部右侧菜单 -->
                <ul class="layui-nav top_menu">
                    <!--<li class="layui-nav-item showNotice" id="showNotice" pc>
                <a href="javascript:;"><i class="iconfont icon-gonggao"></i><cite>系统公告</cite></a>
            </li>-->
                    <!--<li class="layui-nav-item" pc>
                <a href="javascript:;" data-url="page/user/changePwd.html">
                    <i class="iconfont icon-shezhi1" data-icon="icon-shezhi1"></i><cite>设置</cite>
                </a>
            </li>-->
                    <li class="layui-nav-item" pc>
                        <a href="/admin/acc/exit" class="toEdit"><i class="iconfont icon-loginout2"></i> <cite>退出</cite></a>
                    </li>
                    <li class="layui-nav-item lockcms" pc>
                        <a href="javascript:;"><i class="iconfont icon-lock1"></i><cite>锁屏</cite></a>
                    </li>
                </ul>

            </div>
        </div>

        <!-- 左侧导航 -->
        <div class="layui-side layui-bg-black">
            <div class="user-photo">
                <a class="img" title="我的头像"><img src="/img/cat/1.jpeg"></a>
                <p>你好！<span class="userName">尊敬的root, 欢迎登录</p>
            </div>
            <div class="navBar layui-side-scroll"></div>
        </div>




        <!-- 右侧内容 默认面板 -->
        <div class="layui-body layui-form">
            <div class="layui-tab marg0" lay-filter="bodyTab">
                <ul class="layui-tab-title top_tab">
                    <li class="layui-this" lay-id="">
                        <i class="iconfont icon-computer"></i>
                        <cite>后台首页</cite>
                    </li>
                </ul>
                <div class="layui-tab-content clildFrame">
                    <div class="layui-tab-item layui-show">

                        <!-- 登录后导入的第一页 -->
                        <iframe src="first"></iframe>

                    </div>
                </div>
            </div>
        </div>



        <!-- 底部 -->
        <div class="layui-footer footer">
            <p>copyright @2018 点卡通后台
                <!--<a href="http://www.mycodes.net/" target="_blank">源码之家</a>-->
            </p>
        </div>
    </div>

    <!-- 锁屏 -->
    <div class="admin-header-lock" id="lock-box" style="display: none;">
        <div class="admin-header-lock-img"><img src="/img/cat/1.jpeg" /></div>
        <div class="admin-header-lock-name" id="lockUserName">root</div>
        <div class="input_btn">
            <input type="password" class="admin-header-lock-input layui-input" placeholder="请输入密码解锁.." name="lockPwd" id="lockPwd" />
            <button class="layui-btn" id="unlock">解锁</button>
        </div>
        <p>请输入正确密码，否则不会解锁成功哦！！！</p>
    </div>

    <!-- 移动导航 -->
    <div class="site-tree-mobile layui-hide"><i class="layui-icon">&#xe602;</i></div>
    <div class="site-mobile-shade"></div>

    <script type="text/javascript" src="/admin/layui/layui.js"></script>
    <script type="text/javascript" src="/admin/js/nav.js"></script>
    <script type="text/javascript" src="/admin/js/leftNav.js"></script>
    <script type="text/javascript" src="/admin/js/index.js"></script>
    <script src="/kj/jquery-3.2.1.js"></script>
    <script>

    $(".toEdit").click(function () {


    })

    </script>
</body>

</html>