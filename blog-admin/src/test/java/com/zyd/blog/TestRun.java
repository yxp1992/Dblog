package com.zyd.blog;

import com.alibaba.fastjson.JSONObject;
import com.zyd.blog.service.BizSmalltalkService;
import com.zyd.blog.service.SysUserService;
import com.zyd.blog.controller.api.SmalltalkController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WebAppConfiguration
public class TestRun {

    @Autowired
    SysUserService sysUserService;      //用户 业务接口

    @Autowired
    BizSmalltalkService bizSmalltalkService;    // 说说业务服务类 --end


    @Autowired
    private WebApplicationContext context;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    SmalltalkController smalltalkController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(smalltalkController).build();
    }

    @Test
    public void test() throws Exception {
        method();
    }
    //
    public void method() throws Exception {
        MvcResult result = mockMvc.perform(post(
                "/Smalltalk/smalltalkForward")
                .param("username", "test")
                .param("password", "test")
                .contentType(MediaType.APPLICATION_JSON).content(JSONObject.toJSONString(null)))
                .andExpect(status().isOk()).andReturn();
        System.out.println("====");
        System.out.println(result.getResponse().getContentAsString());
    }


}
