// JavaScript Document

<script type="text/javascript">  
    var wrap = document.getElementById("wrap");  
      
    var divHeight = window.innerHeight;  
      
    wrap.style.height = divHeight + "px";  
  
    var content = $(".content");//懒得写获取类的原生js代码了，直接用了jquery，=。=  
  
    content.height(divHeight);  
  
    var startTime = 0, //开始翻屏时间  
        endTime = 0,  
        now = 0;     
  
    if ((navigator.userAgent.toLowerCase().indexOf("firefox")!=-1)){  
        //for firefox;  
        document.addEventListener("DOMMouseScroll",scrollFun,false);  
      
    }  
    else if (document.addEventListener) {  
      
        document.addEventListener("mousewheel",scrollFun,false);  
      
    }  
    else if (document.attachEvent) {  
      
        document.attachEvent("onmousewheel",scrollFun);  
      
    }  
    else{  
      
        document.onmousewheel = scrollFun;  
      
    }  
  
    //滚动事件处理函数  
    function scrollFun(event){  
  
            startTime = new Date().getTime();  
  
            var delta = event.detail || (-event.wheelDelta);  
  
            if ((endTime - startTime) < -1000) {  
                //1秒内执行一次翻页  
  
                if (delta > 0 && parseInt(main.style.top) > -divHeight * ( content.length - 1)) { //向下翻页  
  
                    now += divHeight ;  
  
                    turnPage(now);  
  
                }   
  
               if (delta < 0 && parseInt(main.style.top) < 0) { //向上翻页  
  
                   now -= divHeight ;  
 
                    turnPage(now);  
  
                }  
  
               endTime = new Date().getTime();  
  
            }  
            else{  
  
               event.preventDefault();  
  
            }  
              
    }  
  
    //翻页函数  
    function turnPage(now){  
          
        $("#main").animate({top:(-now+'px')},1000);  
  
        //懒得写动画代码了，直接用了jquery</span><span style="font-size:14px;">，=。=  
    }  
</script>
