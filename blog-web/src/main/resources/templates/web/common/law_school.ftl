<!-- 模块-法学院 -->
<nav class="navbar TitleName must_hide">
    <div class="container">
        <div class="navbar-header">
            <h1>法学院</h1>
            <span>Law School </span>
        </div>
        <p class="navbar-text navbar-right"><a href="#" class="navbar-link">MORE > </a></p>
    </div>
</nav>
<div class="container school">
    <div class="row">

        <div class="col-sm-6 col-md-3" v-for="video in video_List">
            <div class="thumbnail">
                <!-- 播放按钮 -->
                <div class="play_box">
                    <div class="play_btn">
                        <div class="play_btn_x"></div>
                    </div>
                </div>
                <a :href="video.videoUrl" target="_blank"><img :src="video.coverUrl" :alt="video.title"></a>
                <div class="caption">
                    <h3>{{video.title}} </h3>
                    <ul class="list-inline">
                        <li><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> {{video.seeCount}}</li>
                        <li><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> {{video.commentCount}}</li>
                        <li class="pull-right">{{video.price==0?'免费':'￥ '+video.price}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <img src="/img/in/wushu.jpg" alt="暂无数据" v-if="video_List.length==0">

    </div>
</div>
<style type="text/css">
    .caption h3{font-weight:400;line-height: 3em; }
    .thumbnail a img{width: 100%; height: 168px;}
    .thumbnail{position: relative;}
    .play_box{
        width: 100%;
        height: 168px;
        position: absolute;
        z-index: 999;
        pointer-events: none;
    }
    .play_btn{
        width: 45px;
        height: 45px;
        border: 6px rgba(256,256,256,0.8) solid;
        border-radius: 50%;
        position:absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        margin:auto;
    }
    .play_btn_x{
        width: 0px;
        height: 0px;
        position: absolute;
        top: 9px;
        left: 12px;
        border: 8px rgba(0,0,0,0) solid;
        border-left: 13px rgba(256,256,256,0.8) solid;
    }
</style>
<script>
    var ws_video_list = new Vue({
        el: '.school',
        data: {
            video_List: []
        }
    })


</script>