<!-- 法库 -->
<div class="jumbotron">
    <nav class="navbar TitleName">
        <div class="container">
            <div class="navbar-header">
                <h1>法库</h1>
                <span>law library  </span>
            </div>
            <p class="navbar-text navbar-right"><a href="#" class="navbar-link">MORE > </a></p>
        </div>
    </nav>
    <div class="container">
        <div class="row sayBoxTu">
            <div class="col-md-3">
                <button type="button" class="btn btn-default btn-lg btn-block"><img class="center-block" src="/dist_jiansu/img/fg-a.png" width="80" height="80"> 法律法规</button>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-default btn-lg btn-block"><img class="center-block" src="/dist_jiansu/img/ws-a.png" width="80" height="80"> 司法解释</button>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-default btn-lg btn-block"><img class="center-block" src="/dist_jiansu/img/al-a.png" width="80" height="80"> 经典案例</button>
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-default btn-lg btn-block"><img class="center-block" src="/dist_jiansu/img/sw-a.png" width="80" height="80"> 办案实务</button>
            </div>
        </div>
    </div>
</div>