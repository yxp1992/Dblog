<!-- 模块-大图轮播 -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" v-for="(data,index) in imgList" :data-slide-to="index"  :class="isActive(index)"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div v-for="(data,index) in imgList" :class="'item '+isActive(index)">
            <img :src="data.imgSrc" @click="openPage(data.aHref)" style="cursor: pointer"/>
        </div>
    </div>
</div>
<script>
    var myCarousel = new Vue({
        el: '#myCarousel',
        data: {
            imgList: [],
            isActive: function (index) {
                return (index == 0) ? 'active' : '';
            }
        },
        methods: {
            openPage: function (url) {
                window.open(url);
            }
        },
        created: function () {
            $.ajax2('/sysLoopPlay/getListByCondition',{},function (res) {
                myCarousel.imgList = res.data;
            })
        }
    })
</script>