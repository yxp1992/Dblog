<!-- 模块-说说 -->
<div class="jumbotron sayBox">
    <div class="container">
        <nav class="navbar TitleName">
            <div class="container">
                <div class="navbar-header">
                    <h1>说说</h1>
                    <span>Say Something </span>
                </div>
                <p class="navbar-text navbar-right"><a href="#" class="navbar-link">MORE > </a></p>
            </div>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="thumbnail">
                    <div class="caption">
                        <h4><img src="/dist_jiansu/img/ren1.png" class="img-circle"> 涓涓清流 <span>2015-12-08</span> </h4>
                        <h3>什么是公诉案件</h3>
                        <p>所谓公诉案件，亦即刑事公诉案件，是指由各级检察机关依照法律相关规定，代表国家追究被告人的刑事责任而提起诉讼的案件...</p>
                        <ul class="list-inline">
                            <li><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 1648</li>
                            <li><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> 39</li>
                            <li><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> 33</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="thumbnail">
                    <div class="caption">
                        <h4><img src="/dist_jiansu/img/ren1.png" class="img-circle"> 擎天柱 <span>2015-12-08</span> </h4>
                        <h3>公诉你不知道的那些事</h3>
                        <p>换押证是一种刑事诉讼法律文书，用来明确羁押状态的犯罪嫌疑人(或被告人)在各刑事诉讼过程中被羁押的期间，以此监督各刑事诉讼...</p>
                        <ul class="list-inline">
                            <li><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 1648</li>
                            <li><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> 39</li>
                            <li><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> 33</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb text-center">
                    <li class="on">01</li>
                    <li>67</li>
                </ol>
            </div>
        </div>
    </div>
</div>