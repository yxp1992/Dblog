<!-- 模块-最新观点 -->
<nav class="navbar TitleName">
    <div class="container">
        <div class="navbar-header">
            <h1>最新观点</h1>
            <span>View</span>
        </div>
        <p class="navbar-text navbar-right"><a href="#" class="navbar-link">MORE > </a></p>
    </div>
</nav>
<div class="container viewBox">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="media media-view">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" src="/dist_jiansu/img/portrait.png" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">论坛大师对话系列：富媒体时代的人际关系图谱分析</h4>
                    <p>近年来，数字图书馆是查阅各类知识的资源中心，它对扩大公诉人知识的广深，增加知识理解的深度，具有不可替代的作用...</p>
                </div>
            </div>
        </div>
        <div class="col-md-1 viewNavigation">
            <ul>
                <li class="active"><a href="#">01</a><span></span></li>
                <li><a href="#">02</a><span></span></li>
                <li><a href="#">03</a><span></span></li>
                <li><a href="#">04</a><span></span></li>
                <li><a href="#">05</a><span></span></li>
            </ul>
        </div>
    </div>
</div>