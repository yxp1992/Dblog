<!doctype html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-eequiv="X-UA-Compatible" content="IE=edge"> <!--是IE8的一个专有属性，它告诉IE8采用何种IE版本去渲染网页 -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>简诉,全国公诉人交流的平台,公诉领域专业数据融合与深度知识挖掘的平台,公诉人风采展示的平台,
        公诉人在线业务学习的平台 - 检察官,律师,法官,知名学者,获取法律法规,经典案例,检察文书快速检索,公诉话题讨论</title>

<#include "/web/layout2/import_resources.ftl">

    <link href="/dist_jiansu/css/jianshu.css" rel="stylesheet">
    <link href="/dist_jiansu/css/carousel.css" rel="stylesheet">

    <style>

        .args_box{width: 1130px;margin: 30px auto;}

        .sousuo{width: 400px; margin-left: 30px;margin-top: 10px; float: left;}
        .sou{
            width: 300px;
            height: 35px;
            display: inline-block;
            text-indent: 0.3em;
        }
        .enter{margin-left: -30px;position:relative;top: 2px;}
        .enter:hover{cursor: pointer;color: blue;}

        /*筛选栏*/
        .shai{
            list-style-type: none;
            clear: both;
        }

        .shai li{
            border-bottom: 1px #ddd solid;
            min-height: 35px;
            line-height: 30px;
            margin: 5px 0px;
            overflow: hidden;
        }
        .shai li h4{
            float: left;
            width: 100px;
            height: auto;
            font-size: 15px;
            text-align: left;
            margin-top: 5px;
        }
        .shai li span{margin: 0px 12px;}

        /* a3筛选栏里面的超链接样式*/
        .shai li a{color:#666;text-decoration: none;}
        /*--被悬浮的，和被选择的*/
        .shai li a:hover{color:#44F;}
        .shai li .ze{color:#44F;}

        /* 标题样式调整 */
        .must_hide{display: none;}
        .copy_fuge{margin-left: -18px;}

        /* 分页 */
        .fenyefox{width: 1130px;margin: 20px auto;}
        .fenyefox *{border-radius: 0px !important;clear: both;}
        #fenye span{background-color: rgba(0,0,0,0);color: #000;}


    </style>

</head>
<body>

<!-- import 头部导航栏 -->
<#include "/web/common/header.ftl">


<!-- 所有搜索参数 -->
<div class="args_box">

    <!-- 复制福哥的标题代码，把搜索框塞进去 -->
    <nav class="navbar TitleName copy_fuge">
        <div class="container">
            <div class="navbar-header">
                <h1>法学院</h1>
                <span>Law School </span>
            </div>
            <div class="sousuo">
                <input class="sou layui-input" placeholder="标题检索" v-model="curr.title" @keyup.enter="f5()"/>
                <i class="enter layui-icon" @click="f5()">&#xe615;</i>
            </div>
            <#--<p class="navbar-text navbar-right"><a href="#" class="navbar-link">MORE > </a></p>-->
        </div>
    </nav>

    <!--筛选栏-->
    <ul class="shai">

        <li v-for="(arg, key, index) in args">
            <h4>{{arg.title}}</h4>
            <span v-for="op in arg.option">
                <a href='javascript:;' @click="f5(key,op.value)" :class="(curr[key]==op.value) ? 'ze' : ''">{{op.text}}</a>
            </span>
        </li>

    </ul>
</div>

<!-- import 法学院 -->
<#include "/web/common/law_school.ftl">

<br />
<!--分页-->
<div class="fenyefox"><div id="fenye"></div></div>


<!-- import 底部 -->
<#include "/web/common/footer.ftl">
<script>

    var args_box = new Vue({
        el: '.args_box',
        data: {
            curr: {
                title: '',
                is_delicate: -1,
                is_not_price: -1,
                orderWay: 0,
            },
            args: {
                is_delicate:{
                    title: '是否精品:',
                    option: [
                        {text: '不限', value: -1},
                        {text: '精品', value: 0},
                        {text: '非精品', value: 1}
                    ]
                },
                is_not_price:{
                    title: '是否免费:',
                    option: [
                        {text: '不限', value: -1},
                        {text: '免费', value: 0},
                        {text: '非免费', value: 1}
                    ]
                },
                orderWay:{
                    title: '综合排序:',
                    option: [
                        {text: '默认', value: 0},
                        {text: '发布时间', value: 1},
                        {text: '观看数量', value: 2},
                        {text: '评论数量', value: 3}
                    ]
                }
            }
        },
        methods: {
            // 刷新数据，key为点击项,value为点击值，str为追加参数
            f5: function (key, value, str) {
                if(key){
                    this.curr[key] = value;
                }
                var url = '/bizLawSchoolVideo/getListByParam' + (str || '');
                $.ajax2(url, this.curr, function (res) {
                    //alert(JSON.stringify(res))
                    if(window.ws_video_list){
                        window.ws_video_list.video_List = res.data.dataList;
                    }
                    // str有值(分页刷新)-页面回顶，str无值(参数刷新)-重绘分页控件
                    if(str){
                        scrollTo(0,0);
                    }else{
                        args_box.f5_page(res.data.page);
                    }

                })
            },
            f5_page: function (page) {  // 刷新分页控件
                //分页
                layui.laypage.render({
                    elem:'fenye',
                    count: page.count,
                    curr: page.pageNo,
                    limits: [4,8,16,32,50,100],
                    limit: page.pageSize,
                    layout: [ 'count', 'prev', 'page', 'next', 'limit', 'skip'],
                    theme: '#1E9FFF',
                    jump: function(obj,first) {
                        if(!first){
                            args_box.f5(false,false,"?pageNo=" + obj.curr + "&pageSize=" + obj.limit);
                        }
                    }
                });

            }
        },
        created: function () {
            this.f5();
        }
    })





</script>

</body>
</html>
