<!doctype html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8">
		<title>简诉,全国公诉人交流的平台,公诉领域专业数据融合与深度知识挖掘的平台,公诉人风采展示的平台, 公诉人在线业务学习的平台 - 检察官,律师,法官,知名学者,获取法律法规,经典案例,检察文书快速检索,公诉话题讨论</title>

		<#include "/web/in/jslib.ftl">

		<link href="/dist_jiansu/css/jianshu.css" rel="stylesheet">
		<link href="/dist_jiansu/css/carousel.css" rel="stylesheet">

	</head>

	<body>

		<!-- import 头部导航栏 -->
		<#include "/web/common/header.ftl">

		<!-- import 大图轮播 -->
		<#include "/web/common/img_loop_play.ftl">
		
		<!-- import 最新观点 -->
		<#include "/web/common/viewpoint_new.ftl">
		
		<!-- import 说说 -->
		<#include "/web/common/smalltalk.ftl">
		
		<!-- import 法学院 -->
		<#include "/web/common/law_school.ftl">
		
		<!-- import 法库 -->
		<#include "/web/common/law_lib.ftl">
		
		<!-- import 友情连接 -->
		<#include "/web/common/link_friend.ftl">
		
		<!-- import 底部 -->
		<#include "/web/common/footer.ftl">

	</body>

</html>