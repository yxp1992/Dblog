package com.zyd.blog.service;

import com.zyd.blog.model.BizLawSchoolComment;

import java.util.List;

/**
 * <p>
 * 法学院视频评论表 服务类
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-02
 */
public interface BizLawSchoolCommentService {


    /**
     * 增加一条评论
     * @param content 内容
     * @param video_id 所属视频id
     * @param create_userid 创建人id
     */
    public void add(String content, Long video_id, Long create_userid);


    /**
     * 删除一条,根据id
     */
    public int deleteById(Long id);


    /**
     * 查询,根据条件
     * @param video_id 所属视频ID
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @param pageSize 页大小
     * @return
     */
    public List<BizLawSchoolComment> getListByCondition(Long video_id, String startDate, String endDate, int pageSize);


}
