package com.zyd.blog.service;

import com.zyd.blog.model.BizSmalltalk;
import com.zyd.blog.model.BizSmalltalkComment;
import com.zyd.blog.model.so.BizSmalltalkSO;
import com.zyd.blog.utils.Page;
import com.zyd.blog.utils.PageAndData;

/**
 * <p>
 *  说说业务服务类 --end
 * </p>
 */
public interface BizSmalltalkService{


    // 发表说说,返回主键
    public Long add(BizSmalltalk bizSmalltalk);

    // 获取指定id的说说
    public BizSmalltalk getById(Long id);


    // 删除一条说说
    public int delete(Long id);


    // 指定说说ID点赞
    public int doLike(long id, long userId);

    // 指定说说取消点赞
    public int unLike(long id, long userId);


    // 获取列表，根据指定条件
    public PageAndData getList(BizSmalltalkSO bizSmalltalkSO);


    // 获取指定id说说的转发列表
    public PageAndData getForwardList(int pageNo, int pageSize, long id);



    // ============================== 说说评论相关  =======================================




    // 增加一条评论
    public long addComment(BizSmalltalkComment bizSmalltalkComment);

    // 删除一条评论
    public int delComment(long id);

    // 拉取指定说说的评论
    public PageAndData getCommentList(Page page, long sid, int orederType);




}
