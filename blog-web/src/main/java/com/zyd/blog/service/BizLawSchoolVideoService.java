package com.zyd.blog.service;


import com.zyd.blog.model.BizLawSchoolVideo;
import com.zyd.blog.utils.Page;

import java.util.List;

/**
 * <p>
 * 法学院视频表 服务类
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-02
 */
public interface BizLawSchoolVideoService{
    /**
     * 根据类型获取
     * @param type (1=法律法规,2=司法解释,3=经典案例,4=办案食物)
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @return
     */
    public List<BizLawSchoolVideo> getListByType(Long type, String startDate, String endDate);

    /**
     * 查询，根据参数条件 (所有int或long类型参数，-1代表忽略此条件)
     * @param title 标题(模糊查询)
     * @param type 类型
     * @param is_delicate 是否精品(0=否，1=是)
     * @param is_not_price 是否免费(0=否,1=是)
     * @param order_way 排序条件(1=上传时间倒叙,2=观看数量倒叙,3=评论数量倒叙)
     * @param page 分页
     * @return
     */
    public List<BizLawSchoolVideo> getListByParam(String title, Long type, int is_delicate, int is_not_price, int order_way, Page page);

    /**
     * 查询总数，根据参数条件 (所有int或long类型参数，-1代表忽略此条件)
     * @param title 标题(模糊查询)
     * @param type 类型
     * @param is_delicate 是否精品(0=否，1=是)
     * @param is_not_price 是否免费(0=否,1=是)
     * @return
     */
    public int getCountByParam(String title, Long type, int is_delicate, int is_not_price);



    /**
     * 根据ID获取
     */
    public BizLawSchoolVideo getById(Long id);

    /**
     * 发布一条法库，需要提供：
     *  title 标题
     *  videoUrl 视频地址
     *  coverUrl 封面地址
     *  type 类型
     *  createUserid 创建人userid
     * @return
     */
    public Long add(BizLawSchoolVideo bizLawSchoolVideo);

    /**
     * 删除一条，根据主键
     */
    public int deleteById(Long id);

}
