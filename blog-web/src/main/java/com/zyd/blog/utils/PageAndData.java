package com.zyd.blog.utils;

import java.io.Serializable;

/**
 * Page 和 Data
 */
public class PageAndData implements Serializable{


    // kk_kk
    Page page;      // 分页信息
    Object dataList;    // 数据列表




    public Object getDataList() {
        return dataList;
    }

    public PageAndData setDataList(Object dataList) {
        this.dataList = dataList;
        return this;
    }

    public Page getPage() {
        return page;
    }

    public PageAndData setPage(Page page) {
        this.page = page;
        return this;
    }


    public PageAndData(Page page, Object dataList){
        this.page = page;
        this.dataList = dataList;
    }


    public static PageAndData get(Page page, Object dataList){
        return new PageAndData(page, dataList);
    }



}
