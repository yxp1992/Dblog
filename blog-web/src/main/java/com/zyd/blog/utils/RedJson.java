package com.zyd.blog.utils;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

/**
 * 业务Json的封装
 */
public class RedJson implements Serializable{


	//
	public int code; 	// 状态码
	public String msg; 	// 描述信息
	public Object data; // 携带对象
	


	/**
	 * 给msg赋值，连缀风格
	 */
	public RedJson setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	/**
	 * 给data赋值，连缀风格
	 */
	public RedJson setData(Object data) {
		this.data = data;
		return this;
	}

	/**
	 * 将data还原为指定类型并返回
	 */
	@SuppressWarnings("unchecked")
	public <T> T getData(Class<T> cs) {
		return (T) data;
	}

	
	/**
	 * 构建一个业务Json,状态码和描述消息
	 */
	public RedJson(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	public RedJson(int code, String msg, Object data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	// 构建一个业务Json
	public static RedJson getJson(int code, String msg) {
		return new RedJson(code, msg);
	}
	// 构建一个业务Json
	public static RedJson getJson(int code, String msg, Object data) {
		return new RedJson(code, msg).setData(data);
	}
	
	// 构建一个业务Json,并且转化为Json字符串
	public static String getJsonStr(int res, String msg) {
		return getJson(res, msg).toString();
	}
	// 构建一个业务Json,并且转化为Json字符串
	public static String getJsonStr(int res, String msg, Object data) {
		return getJson(res, msg, data).toString();
	}

	

	// ================= 序列化 =================

	// 指定k v转json
	private String toKeyValue(String key, Object value) {
		return "\"" + key + "\":" + value + ",";
	}
	
	@Override
	public String toString() {
		return toString(true);
	}

	/**
	 * 序列化为标准JSON字符串，isData代表是否同时系列化obj对象，默认true
	 */
	public String toString(boolean isData) {
		StringBuffer sb = new StringBuffer("{");
		sb.append(toKeyValue("code", code));
		sb.append(toKeyValue("msg", "\"" + msg + "\""));
		if (isData == true) {
			sb.append(toKeyValue("data", data));
		}
		return sb.substring(0, sb.length() - 1) + "}";
	}

	// 快捷方法
	/** 构建成功的json */
	public static RedJson getJsonSuccess(String msg) {
		return new RedJson(200, msg);
	}
	/** 构建成功的json */
	public static RedJson getJsonSuccess(String msg, Object data) {
		return new RedJson(200, msg, data);
	}
	/** 构建失败的json */
	public static RedJson getJsonError(String msg) {
		return new RedJson(500, msg);
	}
	/** 构建成功或失败的json */
	public static RedJson getJsonByLine(int line) {
		if(line > 0){
			return new RedJson(200, "ok");
		}
		return new RedJson(500, "error");
	}

	// 歪门邪道方法

	/**
	 * static快速构造，将其中的'转换为"
	 */
	public static String forStr(String jsonstr) {
		return jsonstr.replaceAll("'", "\"");
	}

	/**
	 * 构建一个业务Json,如果code>0则赋值msg1,否则赋值msg2
	 */
	public static RedJson getJsonIf(int code, String msg1, String msg2) {
		if (code > 0) {
			return new RedJson(code, msg1);
		} else {
			return new RedJson(code, msg2);
		}
	}


	// 并json化data
	public static String getJsonDataStr(int code, String msg, Object data){
		return getJsonStr(code, msg, JSON.toJSONString(data));
	}

	// 并json化data
	public String toString2(){
		data = JSON.toJSONString(data);
		return toString();
	}
	
	
}
