package com.zyd.blog.controller.api;


import com.zyd.blog.model.BizApply;
import com.zyd.blog.utils.FS;
import com.zyd.blog.utils.Page;
import com.zyd.blog.utils.PageAndData;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.webutil.JurUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  申请与恢复--前端控制器
 * </p>
 *
 */
@Slf4j
@RestController
@RequestMapping("/Apply/")
public class BizApplyController {

    // 根据id返回
    @RequestMapping("getById")
    public RedJson getById(long id){
        return RedJson.getJson(200,"ok", FS.bizApplyService.getById(id));
    }

    // 发布一条申请
    @RequestMapping("send")
    public RedJson send(String content, HttpServletRequest request){
        long currUserId = JurUtil.getUid(request);
        return RedJson.getJson(200,"ok", FS.bizApplyService.add(currUserId, content));
    }


    // 处理一条申请
    @RequestMapping("handle")
    public RedJson handle(Long id, int status, String handContent, HttpServletRequest request){
        long currUserId = JurUtil.getUid(request);
        BizApply bizApply = FS.bizApplyService.getById(id);
        if(bizApply.getTo_uid() != currUserId){
            return RedJson.getJson(500,"抱歉，您并非此条申请的待处理人");
        }
        if(bizApply.getStatus() != 1){
            return RedJson.getJson(500,"抱歉，此申请已经处理，不能重复处理");
        }
        // LD
        int a= FS.bizApplyService.handleApply(id, status, handContent);
        return RedJson.getJson(200, "ok", a);
    }




    /**
     * 查询
     * @param way 方式(1=获取我的申请 ,2=获取我的回复)
     * @param is_hand 是否已经处理 （1=是，2=否）
     * @return
     */
    @RequestMapping("getByPage")
    public RedJson getByPage(
            @RequestParam(defaultValue = "1") int pageNo,
            @RequestParam(defaultValue = "10") int pageSize,
            @RequestParam(defaultValue = "1") int way,
            @RequestParam(defaultValue = "0") int is_hand,
            HttpServletRequest request
        ){
        long currUserId = JurUtil.getUid(request);
        Page page = Page.getPage(pageNo, pageSize);
        PageAndData data = FS.bizApplyService.getByWayAndStatus(page, currUserId, way, is_hand);
        return RedJson.getJson(200,"ok", data);
    }



    // 拉取当前登陆用户的关系列表
//    @RequestMapping("/getUserInfo")
//    public ResponseBean  getUserInfo(HttpServletRequest request){
//        Long userId = JWTUtil.getUserIdByReq(request);
//        List<Map> data =sysUsersRelationService.getRelationshipUser(userId);
//        return new ResponseBean(200,"成功", data);
//    }



}

