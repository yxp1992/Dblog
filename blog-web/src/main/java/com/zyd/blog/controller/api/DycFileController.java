package com.zyd.blog.controller.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.nio.file.Paths;

/**
 * Springboot接管的动态资源目录
 */
@Slf4j
@RestController
@RequestMapping("/"+SysImgUploadController.httpDyc)
public class DycFileController {

    @Autowired
    private ResourceLoader resourceLoader;

    @Value("${sysImgUpload.ROOT}")
    private String ROOT;    // 资源所在文件夹，相对路径(相对项目)

    public static String staticFolder = null;    // 资源所在文件夹，绝对路径,通过 ROOT变量 求出来

    // springboot接管静态目录
    @RequestMapping(method = RequestMethod.GET, value = "/{fileway}/{filename:.+}")
    @ResponseBody
    public ResponseEntity<?> getFile(@PathVariable String fileway, @PathVariable String filename) {

        try {
            if(staticFolder == null){
                staticFolder = new File(ROOT).getAbsolutePath()+"/";
            }

            String filePath = Paths.get(staticFolder , fileway, filename).toString();
            log.info("请求文件的地址：" + filePath);
            return ResponseEntity.ok(resourceLoader.getResource("file:" + filePath));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


}
