package com.zyd.blog.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 法学院 - 控制器
 */
@Controller
@RequestMapping("/web/law_school/")
public class LawSchoolControllerW {



    // 跳转到法学院视频中心
    @GetMapping("law_school_video_list")
    public String law_school_video_list() {
        return "/web/law_school/law_school_video_list";
    }


}
