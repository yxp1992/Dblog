package com.zyd.blog.controller.api;

import com.zyd.blog.model.SysUser;
import com.zyd.blog.utils.FS;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.webutil.JurUtil;
import com.zyd.blog.webutil.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping("/acc/")
public class AccController {


    // 处理登录
    @RequestMapping("doLogin")
    public RedJson doLogin(String username, String password, HttpServletRequest request, HttpServletResponse response){
        if(username == null || password == null){
            return RedJson.getJsonError("请提供username和password参数");
        }
        RedJson redJson = FS.accService.doLogin(username, password, WebUtil.getRemoteHost(request));
        if(redJson.code == 200){    // 200代表登陆成功，缓存密钥到客户端，以便以后直接从Redis取值
            long login_uid = redJson.getData(SysUser.class).getId();
            JurUtil.setUid(response, login_uid);
        }
        return redJson;
    }

    // 注销
    @RequestMapping("doExit")
    public RedJson doExit(HttpServletRequest request, HttpServletResponse response){
        JurUtil.removeUid(request, response);
        return RedJson.getJsonSuccess("注销成功");
    }






}
