package com.zyd.blog.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 错误页总配置的地方
 */
@Controller
public class ErrorControllerW {



    // 转到web端 相应错误页
    @RequestMapping("/web/error/{code}")
    public String web_error(@PathVariable int code){
        return "/web/error/" + code;
    }


}
