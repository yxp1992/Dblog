package com.zyd.blog.controller.web;

import com.zyd.blog.webutil.ViewUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.swing.text.View;

/**
 * 项目首页相关跳转
 */
@Controller
@RequestMapping("/web/home/")
public class HomeControllerW {


    /**
     * 首页
     */
    @RequestMapping("index")
    public String index(){
        return ViewUtil.web("/home/index");
    }



}
