package com.zyd.blog.controller.web;

import com.zyd.blog.webutil.ViewUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页跳转部分
 */
@Controller
public class IndexControllerW {

    // 跳到首页
    @RequestMapping({"/"})
    public String index(){
        return ViewUtil.web("/home/index");
    }


    //




}
