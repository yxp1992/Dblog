package com.zyd.blog.controller.api;


import com.zyd.blog.model.BizLawSchoolVideo;
import com.zyd.blog.service.BizLawSchoolVideoService;
import com.zyd.blog.utils.Page;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.webutil.JurUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 法学院视频表 前端控制器
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-02
 */
@Slf4j
@Controller
@RequestMapping("/bizLawSchoolVideo")
public class BizLawSchoolVideoController {


    @Autowired
    BizLawSchoolVideoService bizLawSchoolVideoService;  // 法学院视频接口


    // 路由跳转

    // 跳到增加页面（增加）
    @RequestMapping("/add")
    public String add(HttpServletRequest request){
        request.setAttribute("inspur_token_userid", JurUtil.getUid(request));
        return "law_school/add";
    }





    // 数据获取



    // 根据类型查询法律法规库
    @ResponseBody
    @RequestMapping("/getListByType")
    public RedJson getListByType(Long type, String startDate, String endDate){
        List<BizLawSchoolVideo> data = bizLawSchoolVideoService.getListByType(type,startDate,endDate);
        return new RedJson(200,"成功", data);
    }

    // 根据参数条件查询
    // 返回：dataList：数据列表
    // page: 分页参数
    @ResponseBody
    @RequestMapping("/getListByParam")
    public RedJson getListByParam(
            @RequestParam(defaultValue = "1") int pageNo,
            @RequestParam(defaultValue = "16") int pageSize,
            @RequestParam(defaultValue = "") String title,
            @RequestParam(defaultValue = "-1") Long type,
            @RequestParam(defaultValue = "-1") int is_delicate,
            @RequestParam(defaultValue = "-1") int is_not_price,
            @RequestParam(defaultValue = "1") int orderWay
    ){
        Page page = new Page(pageNo,pageSize);
        Map map = new HashMap();
        map.put("dataList", bizLawSchoolVideoService.getListByParam(title, type, is_delicate, is_not_price, orderWay, page));
        map.put("page", page);
        return new RedJson(200,"成功", map);
    }

    /**
     * 发布一条法库，需要提供：
     *  title 标题
     *  videoUrl 视频地址
     *  coverUrl 封面地址
     *  type 类型
     *  createUserid 创建人userid
     *  price2: 价格(double形式)
     */
    @ResponseBody
    @RequestMapping("/doAdd")
    public RedJson doAdd(BizLawSchoolVideo bizLawSchoolVideo,double price2, HttpServletRequest request){
        try{
            Long userId = JurUtil.getUid(request);
            bizLawSchoolVideo.setCreateUserid(userId);          // 创建人
            bizLawSchoolVideo.setPrice(new BigDecimal(price2));  //金额
            Long pkid = bizLawSchoolVideoService.add(bizLawSchoolVideo);
            return new RedJson(200,"插入成功",pkid);
        }catch(Exception e){
            return new RedJson(500,"插入失败："+e.getMessage(),null);
        }
    }

    /**
     * 删除一条
     */
    @ResponseBody
    @RequestMapping("/deleteById")
    public RedJson deleteById(Long id,HttpServletRequest request){
        try{
            Long userId = JurUtil.getUid(request);
            int a = bizLawSchoolVideoService.deleteById(id);
            if(a == 0){
                return new RedJson(500,"删除失败：指定法学院视频id不存在",null);
            }
            log.info("用户"+userId+"删除法学院视频："+id);
            return new RedJson(200,"删除成功",null);
        }catch(Exception e){
            return new RedJson(500,"删除失败："+e.getMessage(),null);
        }
    }






}

