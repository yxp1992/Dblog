package com.zyd.blog.controller.api;


import com.alibaba.fastjson.JSON;
import com.zyd.blog.model.SysLoopPlay;
import com.zyd.blog.service.SysLoopPlayService;
import com.zyd.blog.utils.RedJson;
import com.zyd.blog.webutil.JurUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 轮播图管理表 前端控制器
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-03
 */
@Slf4j
@RestController
@RequestMapping("/sysLoopPlay")
public class SysLoopPlayController {

    @Autowired
    SysLoopPlayService sysLoopPlayService;

    /**
     * 添加一条
     * 需要字段：
     * title：标题
     * imgSrc：图片地址
     * aHref：点击后跳转的超链接
     * isEnable：是否启用(1=是,2=否)
     * type：轮播图位置(1=首页顶部的,2=……)
     */
    @RequestMapping("/add")
    public RedJson add(SysLoopPlay sysLoopPlay, HttpServletRequest request){
        try{
            Long userId = JurUtil.getUid(request);
            int a = sysLoopPlayService.add(sysLoopPlay);
            if(a == 0){
                return new RedJson(500,"添加失败",null);
            }
            log.info("用户"+userId+"添加图片："+sysLoopPlay.toString());
            return new RedJson(200,"添加成功",null);
        }catch(Exception e){
            return new RedJson(500,"添加失败："+e.getMessage(),null);
        }
    }


    /**
     * 删除一条，根据主键
     */
    @RequestMapping("/deleteById")
    public RedJson deleteById(Long id, HttpServletRequest request){
        try{
            Long userId = JurUtil.getUid(request);
            int a = sysLoopPlayService.deleteById(id);
            if(a == 0){
                return new RedJson(500,"删除失败：指定图片"+id+"不存在",null);
            }
            log.info("用户"+userId+"删除图片："+id);
            return new RedJson(200,"删除成功",null);
        }catch(Exception e){
            return new RedJson(500,"删除失败："+e.getMessage(),null);
        }
    }

    /**
     * 修改一条记录(标题、图片地址、超链接跳转地址、条件id)
     * 需要字段：
     * title：标题
     * imgSrc：图片地址
     * aHref：点击后跳转的超链接
     * isEnable：是否启用(1=是,2=否)
     * id：条件Id
     */
    @RequestMapping("/update")
    public RedJson ResponseBean(SysLoopPlay sysLoopPlay, HttpServletRequest request){
        System.out.println("是啥："+sysLoopPlay);
        try{
            Long userId = JurUtil.getUid(request);
            int a = sysLoopPlayService.update(sysLoopPlay);
            if(a == 0){
                return new RedJson(500,"修改失败：指定图片"+sysLoopPlay.getId()+"不存在",null);
            }
            log.info("用户"+userId+"修改图片："+sysLoopPlay.toString());
            return new RedJson(200,"修改成功",null);
        }catch(Exception e){
            return new RedJson(500,"修改失败："+e.getMessage(),null);
        }
    }

    /**
     * 查询,根据条件
     * @param type 类型(1=首页顶部的,2=……) 默认1
     * @param is_enable 是否启用(1=是,2=否) 默认1
     * @return
     */
    @RequestMapping("/getListByCondition")
    public String getListByCondition(@RequestParam(defaultValue = "1")int type, @RequestParam(defaultValue = "1")int is_enable){
        List<SysLoopPlay> data = sysLoopPlayService.getListByCondition(type,is_enable);
        return JSON.toJSONString(new RedJson(200,"成功", data));
    }



    @InitBinder
    private void initBinder(WebDataBinder binder){
        binder.setDisallowedFields("createTime");//由表单到JavaBean赋值过程中哪一个值不进行赋值
    }



}

