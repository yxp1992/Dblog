package com.zyd.blog.webutil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * redis工具类
 *
 */
@Component
public class RedisUtil {
	static StringRedisTemplate stringRedisTemplate;     // 说说表Mapper接口
	static RedisTemplate redisTemplate;     // 说说表Mapper接口
	@Autowired
	public void setStringRedisTemplate(StringRedisTemplate stringRedisTemplates) {
		RedisUtil.stringRedisTemplate = stringRedisTemplates;
	}

//	static RedisTemplate redisTemplates;     // 说说表Mapper接口
//	@Autowired
//	public void setRedisTemplate(RedisTemplate redisTemplate) {
//		RedisUtil.redisTemplates = redisTemplate;
//	}


//	public static StringRedisTemplate stringRedisTemplate = Springs.getBean(StringRedisTemplate.class);
//
//	public static RedisTemplate<String, Object> redisTemplate =  Springs.getBean(RedisTemplate.class);

	/**
	 * 有效期按小时
	 *
	 * @param key
	 * @param value
	 * @param timeout
	 */
	public static void setByHour(String key, String value, int timeout) {
		stringRedisTemplate.opsForValue().set(key, value, timeout, TimeUnit.HOURS);
	}

	/**
	 * 默认7*24小时
	 *
	 * @param key
	 * @param value
	 */
	public static void set(String key, String value) {
		setByHour(key, value, 24 * 7);
	}

	/**
	 *
	 * @param key
	 * @param value
	 * @param timeout
	 */
	public static void setMinutes(String key, String value, int timeout) {
		stringRedisTemplate.opsForValue().set(key, value, timeout, TimeUnit.MINUTES);
	}

	public static String get(String key) {
		return stringRedisTemplate.opsForValue().get(key);
	}

	public static Set<String> keys(String pattern) {
		return stringRedisTemplate.keys(pattern);
	}

	public static void del(String key) {
		stringRedisTemplate.delete(key);
	}

	public static void clearCache() {
		Set<String> keys = stringRedisTemplate.keys("ChargerlinkCache:*");
		stringRedisTemplate.delete(keys);
	}



	/**
	 * 入队
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public static Long rightPush(String key, String value) {
		return stringRedisTemplate.opsForList().rightPush(key, value);
	}

	/**
	 * 第一个元素
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public static Long leftPush(String key, String value) {
		return stringRedisTemplate.opsForList().leftPush(key, value);
	}

	/**
	 * 出队
	 *
	 * @param key
	 * @return
	 */
	public static String leftPop(String key) {
		return stringRedisTemplate.opsForList().leftPop(key);
	}

	/**
	 * 根据key删除vaue
	 *
	 * @param key
	 * @return
	 */
	public static Long remove(String key, String value) {
		return stringRedisTemplate.opsForSet().remove(key, value);
	}

	/**
	 * 栈/队列长
	 *
	 * @param key
	 * @return
	 */
	public static Long length(String key) {
		return stringRedisTemplate.opsForList().size(key);
	}

	/**
	 * 范围检索
	 *
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<String> range(String key, int start, int end) {
		return stringRedisTemplate.opsForList().range(key, start, end);
	}

	private static void opSetAddEle(String key, String val) {
		BoundSetOperations<String, String> ops = stringRedisTemplate.boundSetOps(key);
		ops.add(val);
	}

	/**
	 * 增加操作，value集合，
	 *
	 * @param key
	 * @param set
	 */
	public static void opSetAdd(String key, Set<String> set) {
		for (String val : set) {
			opSetAddEle(key, val);
		}
	}

	/**
	 * key对应的所有值，value为Set
	 *
	 * @param key
	 * @return
	 */
	public static Set<String> opSetGetByKey(String key) {
		BoundSetOperations<String, String> ops = stringRedisTemplate.boundSetOps(key);
		// 查出所有数据
		return ops.members();
	}

	/**
	 * 删除集合的元素
	 *
	 * @param key
	 * @param ele
	 */
	public static void opSetRemoveEle(String key, String ele) {
		BoundSetOperations<String, String> ops = stringRedisTemplate.boundSetOps(key);

		// 替换先前的
		if (ops.isMember(ele)) {
			ops.remove(ele);
		}
	}

	public static void hset(String key, String hk, String hv) {
		BoundHashOperations<String, Object, Object> ops = stringRedisTemplate.boundHashOps(key);
		ops.put(hk, hv);
	}

	public static Object hget(String key, String hk) {
		BoundHashOperations<String, Object, Object> ops = stringRedisTemplate.boundHashOps(key);
		return ops.get(hk);
	}

	public static List<Object> multiGet(String key, Collection<Object> keys) {
		BoundHashOperations<String, Object, Object> ops = stringRedisTemplate.boundHashOps(key);
		return ops.multiGet(keys);
	}

	public static Long hdel(String key, String hk) {
		BoundHashOperations<String, Object, Object> ops = stringRedisTemplate.boundHashOps(key);
		return ops.delete(hk);
	}

	public static Map<Object, Object> entries(String key) {
		BoundHashOperations<String, Object, Object> ops = stringRedisTemplate.boundHashOps(key);
		return ops.entries();
	}



	public static void addList(String key, Object... values) {
		BoundListOperations<String, Object> ops = redisTemplate.boundListOps(key);
		ops.leftPushAll(values);
	}

	public static List getList(String key) {
		BoundListOperations<String, Object> ops = redisTemplate.boundListOps(key);
		return ops.range(0, -1);
	}

	public static void getList(String key, int startIndex, int endIndex) {
		BoundListOperations<String, Object> ops = redisTemplate.boundListOps(key);
		ops.range(0, -1);
	}

	public static void putHash(String key, String hashKey, Object value) {

		redisTemplate.opsForHash().put(key, hashKey, value);

	}

	public static Object getHashValue(String key, String hashKey) {
		return redisTemplate.opsForHash().get(key, hashKey);
	}

	public static void deleteHash(String key, Set<Object> hashKeys) {
		if (hashKeys == null) {
			return;
		}
		Object[] keys = hashKeys.toArray(new Object[hashKeys.size()]);
		redisTemplate.opsForHash().delete(key, keys);
	}

	public static List<Object> getHashValues(String key) {

		return redisTemplate.opsForHash().values(key);
	}

	public static List<Object> getHashValues(String key, Set<Object> keys) {
		return redisTemplate.opsForHash().multiGet(key, keys);
	}

	public static Set<Object> getHashKeys(String key) {
		return redisTemplate.opsForHash().keys(key);
	}
	/**
	 * 写入数据（不存在时写入成功） 判断key是否存在，存在返回false，不存在返回true，并存储到redis中，进行定时缓存
	 * @author zhiqiang.guo
	 * @param key
	 * @param value
	 * @return
	 */
	public static boolean setNx(final String key,final String value,final int time){
		return stringRedisTemplate.execute(new RedisCallback<Boolean>() {
			@Override
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				boolean flag = connection.setNX(key.getBytes(), value.getBytes());
				//如果写入成功了，需要给这条数据加时效
				if(flag){
					connection.expire(key.getBytes(), time);
				}
				return flag;
			}
		});
	}
}
