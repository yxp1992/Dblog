package com.zyd.blog.webutil;

/**
 * 权限异常
 */
public class RoleException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6806129545290130142L;
	
	private int type;
	

	/**
	 * @return 获得异常类型
	 */
	public int getType() {
		return type;
	}

	/**
	 * 只允许以下几种异常
	 */
	public RoleException(int type,String s) {
        super(s);
        this.type=type;
    }
	

}
