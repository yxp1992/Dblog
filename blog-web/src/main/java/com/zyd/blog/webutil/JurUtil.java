package com.zyd.blog.webutil;

import com.zyd.blog.utils.FS;
import com.zyd.blog.utils.NbUtil;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 权限验证工具类
 */
public class JurUtil {

	
	public static String tokenName = "inspur_token";		// cookie的名字
	public static int tokenTime = 30 * 24 * 60 * 60;		// cookie失效时间，单位s 此为30天
	
	
	
	/**
	 * 在指定客户端登陆上指定userId
	 */
	public static void setUid(HttpServletResponse response, long uid){
		String tokenValue = Md5Util.MD5(NbUtil.getMarking28());
		System.out.println("生成的tokenValue："+tokenValue);
		CookieUtil.addCookie(response, tokenName, tokenValue, "/", tokenTime);	// 客户端注入cookie
		RedisUtil.set(tokenValue, String.valueOf(uid));	 // redis上注入键值
	}

	/**
	 * 在指定客户端清除userId状态,清楚cookie和redis缓存
	 */
	public static void removeUid(HttpServletRequest request, HttpServletResponse response){
		Cookie cookie = CookieUtil.getCookie(request, tokenName);
		if(cookie == null){
			return;
		}
		String tokenValue = cookie.getValue();
		CookieUtil.delCookie(request,response, tokenName);	// 清除cookie
		RedisUtil.del(tokenValue);	// 清除redis缓存
	}


	/**
	 * 返回当前userId，未登录返回0
	 * @return
	 */
	public static long getUid(HttpServletRequest request){
		try {
			Cookie cookie = CookieUtil.getCookie(request, tokenName);
			if(cookie == null){
				return 0;
			}
			String tokenValue = cookie.getValue();
			String uidString = RedisUtil.get(tokenValue);
			return (uidString == null ? 0 :  Long.valueOf(uidString));
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * 根据userId 获取 username
	 */
	public static String getUsername(long userId){
		return FS.sysUserService.getById(userId).getUsername();
	}

	/**
	 * 返回当前username,未登录返回null
	 */
	public static String getUsername(HttpServletRequest request){
		long userId = getUid(request);
		return (userId == 0 ? null : getUsername(userId)) ;
	}
	
	
	
	
	
	
	
}
