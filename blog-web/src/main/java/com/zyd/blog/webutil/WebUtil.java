package com.zyd.blog.webutil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.*;


/**
 * web工具类
 */
public class WebUtil {

	// 工具方法

	/**
	 * 取出一个值，我保证不乱码,tomcat8及以上版本此方法废掉
	 */
	public static String getParam(HttpServletRequest request, String key) {
		try {
			request.setCharacterEncoding("UTF-8");
			String value = request.getParameter(key); // 获得v
			if (value != null && request.getMethod().equals("GET")) {
				value = new String(value.getBytes("ISO-8859-1"), "UTF-8");
			}
			return value;
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}
	/**
	 * 取出一个值，我保证不乱码,tomcat8及以上版本专用
	 */
	public static String getParam8(HttpServletRequest request, String key) {
		try {
			if(request.getCharacterEncoding()==null) {
				request.setCharacterEncoding("UTF-8");
			}
			return request.getParameter(key);
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}
	/**
	 * 将一个值，强制转码ISO_8859_1-->utf-8
	 */
	public static String getISO_8859_1(String str) {
		try {
			if(str==null) {
				return str;
			}
			return new String(str.getBytes("ISO-8859-1"), "UTF-8");
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return str;
	}

	/**
	 * 将一个request请求所携带的参数封装成map返回
	 * <br/>对于数组型参数，此方法不能正确获得值
	 * @param request
	 * @return
	 */
	public static Map<String,String>getParamsMap(HttpServletRequest request){
		Map<String,String>map=new HashMap<String, String>();
		try {
			Enumeration<String> paramNames = request.getParameterNames();//获得K列表
			request.setCharacterEncoding("UTF-8");
			while (paramNames.hasMoreElements()) {  
				try {
					String key =paramNames.nextElement();	//获得k
					String value=request.getParameter(key);	//获得v
					if(request.getMethod().equals("GET")){
						value=new String(value.getBytes("ISO-8859-1"),"UTF-8");
					}
					map.put(key,value);	
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 返回请求端的IP地址
	 */
	public static String getRemoteHost(HttpServletRequest request) {
		String ip = request.getRemoteHost() ;
		/*if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}*/
		return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
	}

	/**
	 * 返回指定地址在服务器上的绝对路径
	 */
	public static String getRealPath(HttpSession session, String path) {
		return session.getServletContext().getRealPath(path);// 路径
	}

	
	
	/**
	 * 检测request请求是否为ajax
	 */
	public static boolean isAjax(HttpServletRequest request) {
		return !(request.getHeader("x-requested-with") == null);
	}

	
	/**
	 * 指定session指定key的Integer集合
	 */
	public static List<Integer> getListByKey(HttpSession session,String key) {
		if (session.getAttribute(key) == null) {
			session.setAttribute(key, new ArrayList<Integer>());
		}
		@SuppressWarnings("unchecked")
		List<Integer> pzcs = (List<Integer>) session.getAttribute(key);
		return pzcs;
	}
	
	
	

//	/**
//	 * 为指定uid设置默认的头像
//	 *
//	 * @param uid
//	 *            用户id
//	 * @param serverPath
//	 *            服务器根路径
//	 * @return 1成功 -1失败
//	 */
//	public static int setDefaultImgHead(int uid) {
//		//随机一个头像
//		int no=(int)(Math.random()*25)+1;
//		File ftou=new File(SysCfg.pathColor+"color/default_head/"+no+".jpeg");//随机头像源路径
//		File fuser=new File( SysCfg.pathColor+ SysCfg.path_img_head+"/"+uid+".png");
//		try {
//			Files.copy(ftou.toPath(),fuser.toPath());//复制
//			return 1;
//		} catch (Exception e) {
//			System.out.print("\n\n用户"+uid+"复制头像时发生了非常重大的异常\n\n"+e.getMessage());
//			e.printStackTrace();
//			return -1;
//		}
//	}

	
	
}
