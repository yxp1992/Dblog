package com.zyd.blog.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * K: 用户之间关系表（sys_uesrs_relation）,必须有关注与被关注的关系 end
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-06-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUsersRelation implements Serializable {
//    private SysUsersRelation sysUsersRelation;
//
//    public SysUsersRelation() {
//        this.sysUsersRelation = new SysUsersRelation();
//    }
//
//    public SysUsersRelation(SysUsersRelation sysUsersRelation) {
//        this.sysUsersRelation = sysUsersRelation;
//    }
    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    private Long userId;
    /**
     * 被关注者编号
     */
    private Long followId;
    /**
     * 关系类型（0，粉丝；1，关注）
     */
    private Long type;
    /**
     * 添加时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
//    @JsonIgnore
//    public SysUsersRelation getSysUsersRelation() {
//        return this.sysUsersRelation;
//    }

}
