package com.zyd.blog.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 消息通知表实体类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private long create_uid;    // 消息产生人uid
    // private String username;    // 消息产生人的username（为了业务而冗余的字段）
    private long to_uid;        // 被通知的人
    private String content;       // 通知内容
    private long goto_id;        // 转到详情时的id索引
    private int is_Read;        // 是否已阅
    private int type;           // 类型
    private Date create_time;   // 创建日期


    //
    public SysMessage(){}

    /**
     * {create_uid}, #{to_uid}, #{content}, #{goto_id}, #{type}
     * @param create_uid 消息产生人uid
     * @param to_uid 被通知的人
     * @param content 通知内容
     * @param type 类型
     * @param goto_id 转到详情时的id索引
     */
    public SysMessage(long create_uid, long to_uid, String content, int type, long goto_id) {
        this.create_uid = create_uid;
        this.to_uid = to_uid;
        this.content = content;
        this.type = type;
        this.goto_id = goto_id;
    }

}
