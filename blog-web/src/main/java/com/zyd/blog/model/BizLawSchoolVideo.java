package com.zyd.blog.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 法学院视频表
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BizLawSchoolVideo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 视频url地址
     */
    private String videoUrl;
    /**
     * 封面url地址
     */
    private String coverUrl;
    /**
     * 分类
     */
    private String type;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 观看数量
     */
    private Integer seeCount;
    /**
     * 评论数量
     */
    private Integer commentCount;
    /**
     * 是否精品(0=否，1=是)
     */
    private Integer isDelicate;
    /**
     * 是否删除(0=未删除，1=已删除)
     */
    private Integer isDelete;
    /**
     * 创建人
     */
    private Long createUserid;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;


}
