package com.zyd.blog.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 法学院视频评论表
 * </p>
 *
 * @author yuxiaopeng
 * @since 2018-07-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BizLawSchoolComment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 内容
     */
    private String content;
    /**
     * 所属法学院视频ID
     */
    private String videoId;
    /**
     * 创建人
     */
    private Long createUserid;
    /**
     * 创建时间
     */
    @JSONField(format="yyyy-MM-dd hh:mm")
    private Date createTime;



    // 额外字段
    private String username;    // 发表人名字
    private String avatar;      // 发表人头像地址





}
