package com.zyd.blog.model.so;

import lombok.Data;

import java.io.Serializable;

/**
 * 说说表，查询条件封装
 */
@Data
public class BizSmalltalkSO implements Serializable {


    private int pageNo = 1;     // 当前页
    private int pageSize = 10;   // 页大小

    private long user_id;           // 限定发表人Id
    private String wd;             // 模糊搜索说说内容
    private int is_original;        // 是否原创(1=是，2=否，0=忽略此条件)
    private int type;               // 说说类型(1=普通,2=图片,3=视频,0=忽略此条件)
    private String start_time;      // 最早时间
    private String end_time;        // 最晚时间
    private int orderType = 1;      // 排序方式(1=id倒序，2=发表日期倒序，3=点赞倒序，4=浏览数量倒序，5=转发数量倒序，6=评论数量倒序),默认1
    private String orderStr;       // 排序方式字符串

    public String getOrderStr() {
        String str = new String[]{ "id", "id", "create_time", "like_count", "browse_count", "forward_count", "comment_count" }[orderType];
        return orderStr = "order by " + str + " desc ";
    }





}
